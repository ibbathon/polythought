import importlib.util
import inspect
import logging
import os
import re
from datetime import datetime

from pony import orm

from ponyherd.config import Config
from ponyherd.migration import InvalidMigrationError, PonyHerdMigration


class InvalidCommandError(Exception):
    pass


class Herder:
    _config: Config
    _db: orm.Database
    _migrations: dict[str, PonyHerdMigration]

    def __init__(self):
        self._config = Config()

        logging.basicConfig()
        self._logger = logging.getLogger("PonyHerd")
        self._logger.setLevel(self._config.log_level)

        self._db = self._config.ponydb
        dsn = os.getenv(self._config.db_dsn)
        if self._db.provider_name is None:
            self._db.bind(provider=self._config.db_provider, dsn=dsn)

        self._migrations = self._find_migrations()
        with orm.db_session:
            self._ensure_versions_table()

    @orm.db_session
    def execute(self, *args):
        """main entry point for ponyherd

        with no args, applies all existing unapplied migrations
        """
        # TODO: actually read and respond to args, instead of assuming "migrate"
        if not args:
            for version in sorted(self._migrations.keys()):
                if self._is_applied(version):
                    continue
                self.apply_migration(version)
            return

        if args[0] == "downone":
            for version in reversed(sorted(self._migrations.keys())):
                if not self._is_applied(version):
                    continue
                self.revert_migration(version)
                return

        if args[0] == "down":
            for version in reversed(sorted(self._migrations.keys())):
                if not self._is_applied(version):
                    continue
                self.revert_migration(version)
            return

        if args[0] == "add":
            version_name = args[1]
            self._create_migration(version_name)
            return

        raise InvalidCommandError(f"unrecognized command {args[0]}")

    def get_versions(self) -> tuple[str, ...]:
        return tuple(sorted(self._migrations))

    def _find_migrations(self) -> dict[str, type[PonyHerdMigration]]:
        """locates all migrations in migrations directory

        returns a dict of {version-str: migration-class}
        """
        migrations: dict[str, type[PonyHerdMigration]] = {}
        for migfilepath in sorted(self._config.migrations_path.glob("*.py")):
            migration_found = False
            # import the module based on filepath, without putting it in globals
            module_name = migfilepath.name[:-3]
            spec = importlib.util.spec_from_file_location(module_name, migfilepath)
            if spec is None or spec.loader is None:
                raise InvalidMigrationError(f"unreadable migration file {migfilepath}")
            module = importlib.util.module_from_spec(spec)
            # execute the import
            spec.loader.exec_module(module)
            for var in inspect.getmembers(module, inspect.isclass):
                cls = var[1]
                if not issubclass(cls, PonyHerdMigration) or cls == PonyHerdMigration:
                    continue
                # we now know this var is a strict subclass of PonyHerdMigration
                if migration_found:
                    # we don't support multiple migrations per file
                    raise InvalidMigrationError(
                        f"migration file {migfilepath} has more than one migration"
                    )
                migration_found = True
                migrations[module_name] = cls

        return migrations

    def _is_applied(self, version):
        """checks the ponyherd_versions table for the given version

        returns True if the version row exists and row.applied is True
        returns False otherwise
        """
        is_applied = self._db.execute(
            """
            select applied from ponyherd_versions where version = $version
            """,
            {"version": version},
        ).fetchone()
        if is_applied and is_applied[0]:
            return is_applied
        return False

    def _set_applied(self, version, status):
        """upserts a row in ponyherd_versions for the given version"""
        self._db.execute(
            """
            insert into ponyherd_versions (version, applied)
            values ($version, $status)
            on conflict (version) do update set applied = $status
            """,
            # execute params are explicitly specified, rather than relying on
            # Pony's ability to read globals, so that we can actually write
            # decent unit tests
            {"version": version, "status": status},
        )

    def _ensure_versions_table(self):
        """checks for ponyherd_versions table and creates it if it doesn't exist"""
        PonyHerdMigration(self._db, indent=0)._create_table(
            "ponyherd_versions",
            ("version", "varchar", "primary key"),
            ("applied", "boolean", "default false"),
            if_not_exists=True,
        )

    def _create_migration(self, version_name):
        # filename is vYYYYMMDD_HHMMSS_snake_case_version_name
        filename_parts = []
        curr_time = datetime.now()
        filename_parts.append(curr_time.strftime("%Y%m%d_%H%M%S"))
        filename_parts.append(
            re.sub(
                r"\W",
                "_",
                re.sub(
                    r"(^|\s)(\S*)",
                    lambda match: match.group(1) + match.group(2).lower(),
                    version_name,
                ),
            )
        )
        filename = "v" + "_".join(filename_parts) + ".py"
        filepath = self._config.migrations_path / filename

        # classname is CamelCaseVersionName
        classname = re.sub(
            r"\W",
            "",
            re.sub(
                r"(^|\s)([A-Za-z])(\S*)",
                lambda match: match.group(2).upper() + match.group(3).lower(),
                version_name,
            ),
        )

        # contents are just a pre-filled empty up and down
        contents = f"""
            from ponyherd.migration import PonyHerdMigration
            class {classname}(PonyHerdMigration):
                def up(self): pass
                def down(self): pass
        """.replace(
            "            ", ""
        ).strip()

        with filepath.open("w") as f:
            f.write(contents)

        self._logger.info(f"created migration {filename}")

    @orm.db_session
    def apply_migration(self, version, force=False):
        """applies a single migration by version name

        version: filename of migration minus the extension
        """
        if not force and self._is_applied(version):
            raise InvalidCommandError("migration is already applied")
        migration_cls = self._migrations[version]
        self._logger.info(f"applying {version} {migration_cls.__name__}")
        migration_cls(self._db).up()
        self._set_applied(version, True)

    @orm.db_session
    def revert_migration(self, version, force=False):
        """reverts a single migration by version name

        version: filename of migration minus the extension
        """
        if not force and not self._is_applied(version):
            raise InvalidCommandError("migration is not yet applied")
        migration_cls = self._migrations[version]
        self._logger.info(f"reverting {version} {migration_cls.__name__}")
        migration_cls(self._db).down()
        self._set_applied(version, False)
