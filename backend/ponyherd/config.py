import importlib
from pathlib import Path

from pony import orm

from ponyherd.util import InvalidAttrPath, load_attr, load_pyproject


class InvalidConfig(Exception):
    pass


class Config:
    ponydb: orm.Database
    migrations_path: Path
    db_provider: str
    db_dsn: str
    log_level: str

    def __init__(self) -> None:
        # TODO: maybe handle manually provided config params?
        pyproject_cfg = load_pyproject()
        try:
            ponydb_path = pyproject_cfg["ponydb"]
            migrations_dir = pyproject_cfg["migrations"]
            self.db_provider = pyproject_cfg["db-provider"]
            self.db_dsn = pyproject_cfg["db-dsn-env-var"]
        except KeyError as e:
            raise InvalidConfig("missing required config key") from e

        try:
            self.ponydb = load_attr(ponydb_path, orm.Database)
        except InvalidAttrPath as e:
            raise InvalidConfig("invalid ponydb path") from e

        try:
            migrations_module = importlib.import_module(migrations_dir)
            self.migrations_path = Path(migrations_module.__path__[0])
        except ModuleNotFoundError as e:
            raise InvalidConfig("invalid migrations path") from e

        self.log_level = pyproject_cfg.get("log-level") or "INFO"
