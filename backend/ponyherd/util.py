import importlib
import tomllib
from pathlib import Path
from typing import Any


class PyprojectNotFound(Exception):
    pass


class InvalidAttrPath(Exception):
    pass


def load_pyproject() -> dict:
    """finds the nearest pyproject.toml and returns the tool.ponyherd section"""
    path: Path = Path.cwd().resolve()
    search_dirs = [path] + list(path.parents)
    for d in search_dirs:
        pyproject_path = d / "pyproject.toml"

        if not pyproject_path.is_file():
            continue
        try:
            with pyproject_path.open("rb") as f:
                toml = tomllib.load(f)
        except tomllib.TOMLDecodeError:
            continue
        # verify that ponyherd is configured in this pyproject.toml
        try:
            return toml["tool"]["ponyherd"]
        except KeyError:
            continue
    raise PyprojectNotFound


def load_attr(attr_path: str, type_: Any) -> Any:
    """loads and returns the pony db object from a given module/attr path

    attr_path is expected to be in the form "pkg.module:db"
    """
    module_path, _, attr_path = attr_path.partition(":")
    if not module_path or not attr_path:
        raise InvalidAttrPath(
            f"DB object path {attr_path} is invalid. "
            + 'Supply in the form of "pkg.module:db".'
        )

    try:
        module = importlib.import_module(module_path)
    except ModuleNotFoundError as e:
        raise InvalidAttrPath(f"Failed to import module {module_path}") from e

    curr_obj = module
    try:
        for attr in attr_path.split("."):
            curr_obj = getattr(curr_obj, attr)
    except AttributeError:
        raise InvalidAttrPath(f'Failed to find "{attr}" in {module_path}')

    if isinstance(curr_obj, type_):
        return curr_obj

    raise InvalidAttrPath(
        f"Specified path {attr_path} does not point to an object of type "
        + f'"{type_.__name__}"'
    )
