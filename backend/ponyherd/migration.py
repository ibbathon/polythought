import logging
import re

from pony import orm


class InvalidMigrationError(Exception):
    pass


class PonyHerdMigration:
    def __init__(self, db: orm.Database, indent: int = 4):
        self._db = db
        self._indent = " " * indent
        self._logger = logging.getLogger("PonyHerd")

    def up(self):
        raise NotImplementedError

    def down(self):
        raise NotImplementedError

    def _translate_column(self, column: tuple[str, str, str]) -> str:
        name = column[0]
        type_ = column[1]
        constraints = column[2]
        if self._db.provider_name == "sqlite":  # type: ignore
            # sqlite differentiates between timestamp and datetime, but postgres
            # does not have a 'datetime' type, so dynamically translate for
            # sqlite to get consistent results
            if type_ == "timestamp":
                type_ = "datetime"
            # sqlite does some funky stuff with defaults
            # the best way to get these to match between pony's auto and the migs
            # is to just remove quotes on string defaults
            constraints = re.sub(r"default '([^']*)'", r"default \1", constraints)
        return f'"{name}" {type_} {constraints}'

    def _create_table(
        self,
        name: str,
        *columns: tuple[str, str, str],
        constraints: list[str] | None = None,
        if_not_exists: bool = False,
    ):
        if len(columns) == 0:
            raise InvalidMigrationError(f"table {name} must have at least one column")
        try:
            constraint_string = ""
            if constraints:
                constraint_string = ", " + ", ".join(constraints)
            command = (
                f'create table {"if not exists" if if_not_exists else ""} "{name}" ('
                + ", ".join(self._translate_column(c) for c in columns)
                + constraint_string
                + ")"
            )
        except IndexError:
            raise InvalidMigrationError(f"invalid column specifier in table {name}")
        self._log(f"creating table {name}")
        self._execute(command)

    def _drop_table(self, name: str):
        command = f'drop table if exists "{name}"'
        self._log(f"dropping table {name}")
        self._execute(command)

    def _add_column(self, table: str, column: tuple[str, str, str]):
        try:
            command = (
                f'alter table "{table}" add column {self._translate_column(column)}'
            )
        except IndexError:
            raise InvalidMigrationError(
                f"invalid column specifier for new column on table {table}"
            )
        self._log(f"adding column {column[0]} to table {table}")
        self._execute(command)

    def _drop_column(self, table: str, column: str):
        command = f'alter table "{table}" drop column "{column}"'
        self._log(f"dropping column {column} from table {table}")
        self._execute(command)

    def _add_index(
        self,
        table: str,
        column: str,
        index: str | None = None,
        unique: bool = False,
    ):
        if index is None:
            index = f"{table}_{column}_key"
        if unique:
            unique_str = "unique "
        else:
            unique_str = ""

        command = f'create {unique_str}index "{index}" on "{table}" ("{column}")'
        self._log(f"adding {unique_str}index {index}")
        self._execute(command)
        if self._db.provider_name == "postgres" and unique:  # type: ignore
            # postgres needs a constraint in addition to the index
            command = (
                f'alter table "{table}" add constraint "{index}" '
                + f'unique using index "{index}"'
            )
            self._log(f"adding unique constraint {index}")
            self._execute(command)

    def _drop_index(
        self,
        table: str,
        column: str,
        index: str | None = None,
        unique: bool = False,
    ):
        if index is None:
            index = f"{table}_{column}_key"
        if unique:
            unique_str = "unique "
        else:
            unique_str = ""

        if self._db.provider_name == "postgres" and unique:  # type: ignore
            command = f'alter table "{table}" drop constraint "{index}"'
            self._log(f"dropping unique constraint {index}")
            self._execute(command)
        else:
            command = f'drop index "{index}"'
            self._log(f"dropping {unique_str}index {index}")
            self._execute(command)

    def _execute(self, command: str):
        self._log(f"executing {command}", channel=self._logger.debug)
        self._db.execute(command)

    def _log(self, text: str, channel=None):
        if channel is None:
            channel = self._logger.info
        channel(self._indent + text)
