import sys

from ponyherd.herder import Herder

if __name__ == "__main__":
    Herder().execute(*sys.argv[1:])
