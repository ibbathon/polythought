#!/bin/bash

# run all unapplied migrations
python -m ponyherd

uvicorn between_words.main:app --host=0.0.0.0 &
