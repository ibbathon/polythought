import base64

import bcrypt
from pony import orm

from between_words.db_mixins import uuid_with_timestamps

# Unfortunately, pony requires an initialized db object in order to create
# models, so we do so in the models file.
db = orm.Database()


class Word(db.Entity):  # type: ignore
    # sqlite is case-sensitive, and pony uses exact class name
    _table_ = "word"

    word = orm.Required(str)
    dictionary = orm.Required(str)
    orm.PrimaryKey(word, dictionary)


# All entity classes defined after this point will have a UUID ID and timestamps
uuid_with_timestamps()


class User(db.Entity):  # type: ignore
    _table_ = "user"

    username = orm.Required(str, unique=True)
    password_hash = orm.Required(str)
    # password_salt's default is purely to help with test_migrations
    # the actual value will be set by the init
    password_salt = orm.Required(str, sql_default="invalid")
    favorite_fruit = orm.Optional(str, default="lemon")
    sessions: orm.Set = orm.Set("Session")

    def __init__(self, *args, **kwargs):
        """custom init to set password_hash for new users"""
        if kwargs.get("password_hash") is None:
            password = kwargs.pop("password", None) or f"{kwargs.get('username')}123"
            pwsalt, pwhash = self._gen_new_password_hash(password)
            kwargs["password_hash"] = pwhash
            kwargs["password_salt"] = pwsalt
        super().__init__(*args, **kwargs)

    @property
    def password(self):
        return "***"

    @password.setter
    def password(self, new_password: str):
        self.password_salt, self.password_hash = self._gen_new_password_hash(
            new_password
        )

    def _gen_new_password_hash(self, new_password: str) -> tuple[str, str]:
        bytes_new_password = new_password.encode("utf-8")
        salt = bcrypt.gensalt()
        return (
            base64.b64encode(salt).decode(),
            base64.b64encode(bcrypt.hashpw(bytes_new_password, salt)).decode(),
        )

    def validate_password(self, entered_password: str) -> bool:
        bytes_entered_password = entered_password.encode("utf-8")
        return base64.b64decode(self.password_hash) == bcrypt.hashpw(
            bytes_entered_password, base64.b64decode(self.password_salt)
        )


class Session(db.Entity):  # type: ignore
    _table_ = "session"
    user = orm.Required(User)
    token = orm.Required(str)
    valid = orm.Required(bool, default=True)
