from typing import Annotated

from fastapi import APIRouter, Depends
from pony import orm

from between_words.api_models import CurrentUserResponse, UserResponse
from between_words.db_models import User
from between_words.util.auth import get_current_user

router = APIRouter(
    prefix="/users",
    tags=["users"],
)


@router.get("", response_model=list[UserResponse])
async def all_users():
    with orm.db_session:
        users = list(User.select())
    return [UserResponse.model_validate(user) for user in users]


@router.get("/{username}", response_model=UserResponse | CurrentUserResponse)
async def get_user(
    username: str, current_user: Annotated[UserResponse, Depends(get_current_user)]
):
    with orm.db_session:
        user = User.get(username=username)

    if current_user.username == username:
        return CurrentUserResponse.model_validate(user)
    else:
        return UserResponse.model_validate(user)
