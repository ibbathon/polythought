from typing import Annotated

from fastapi import APIRouter, Depends, HTTPException, Response
from fastapi.security import OAuth2PasswordRequestForm
from pony import orm

from between_words.db_models import Session
from between_words.util.auth import (
    create_access_token,
    oauth2_scheme,
    validate_credentials,
)

router = APIRouter(prefix="/auth", tags=["auth"])


@router.post("/login")
async def login(form_data: Annotated[OAuth2PasswordRequestForm, Depends()]):
    user = validate_credentials(form_data.username, form_data.password)
    if not user:
        raise HTTPException(
            status_code=400,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )

    access_token = create_access_token(user)
    return {"access_token": access_token, "token_type": "bearer"}


@router.post("/logout")
async def logout(response: Response, token: Annotated[str, Depends(oauth2_scheme)]):
    with orm.db_session:
        session = Session.get(token=token)
        if session is None:
            raise HTTPException(
                status_code=401,
                detail="Session is already invalidated",
                headers={"WWW-Authenticate": "Bearer"},
            )
        session.valid = False
