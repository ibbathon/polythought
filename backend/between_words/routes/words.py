from fastapi import APIRouter, HTTPException
from pony import orm

from between_words.util.word import random_words

router = APIRouter(
    prefix="/words",
    tags=["words"],
)


@router.get("/random", response_model=list[str])
async def get_random_words(count: int = 1, dictionary: str = "default"):
    if count <= 0:
        raise HTTPException(status_code=422, detail="count must be a positive integer")
    with orm.db_session:
        words = random_words(count, dictionary)
        if not words:
            raise HTTPException(status_code=422, detail="invalid dictionary")
        return words
