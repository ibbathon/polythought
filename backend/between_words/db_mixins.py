import uuid
from datetime import datetime, timezone

from pony import orm

old_init = orm.core.EntityMeta.__init__


def utcnow():
    return datetime.now(timezone.utc)


def uuid_with_timestamps():
    def shared_before_insert(self):
        self.updated_at = self.created_at

    def shared_before_update(self):
        self.updated_at = utcnow()

    def new_init(entity_cls, cls_name, cls_bases, cls_dict):
        entity_cls.uuid = orm.PrimaryKey(uuid.UUID, default=uuid.uuid4)
        entity_cls.created_at = orm.Required(datetime, default=utcnow)
        entity_cls.updated_at = orm.Optional(datetime)
        entity_cls._old_before_insert = entity_cls.before_insert
        entity_cls._old_before_update = entity_cls.before_update

        def joined_before_insert(self):
            shared_before_insert(self)
            self._old_before_insert()

        def joined_before_update(self):
            shared_before_update(self)
            self._old_before_update()

        entity_cls.before_insert = joined_before_insert
        entity_cls.before_update = joined_before_update
        old_init(entity_cls, cls_name, cls_bases, cls_dict)

    orm.core.EntityMeta.__init__ = new_init
