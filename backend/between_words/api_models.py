import uuid
from datetime import datetime

from pydantic import BaseModel, ConfigDict


class BaseDBResponse(BaseModel):
    model_config = ConfigDict(from_attributes=True)

    uuid: uuid.UUID
    created_at: datetime
    updated_at: datetime


class UserResponse(BaseDBResponse):
    username: str


class CurrentUserResponse(UserResponse):
    favorite_fruit: str
