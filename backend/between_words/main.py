import os
from contextlib import asynccontextmanager

from fastapi import APIRouter, Depends, FastAPI
from fastapi.middleware.cors import CORSMiddleware

from between_words.routes import auth, users, words
from between_words.util.auth import get_current_user
from between_words.util.db import init_pony


@asynccontextmanager
async def api_lifespan(app: FastAPI):
    init_pony()
    yield


DEV = os.getenv("FASTAPI_ENV") == "development"
FRONTEND_DOMAIN = os.getenv("FRONTEND_DOMAIN")
ORIGINS = FRONTEND_DOMAIN.split(",") if FRONTEND_DOMAIN else []

# not sure if this is the best way to handle auth vs non-auth routes,
# but at least it works
api_router = APIRouter(prefix="/api")
api_router.include_router(auth.router)

# all future routers should be included in the authenticated_router
authenticated_router = APIRouter(dependencies=[Depends(get_current_user)])
authenticated_router.include_router(users.router)
authenticated_router.include_router(words.router)


app = FastAPI(lifespan=api_lifespan)
api_router.include_router(authenticated_router)
app.include_router(api_router)
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"] if DEV else ORIGINS,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
