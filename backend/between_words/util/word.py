from between_words.db_models import Word


def random_words(count: int = 1, dictionary: str = "default"):
    return list(w.word for w in Word.select(dictionary=dictionary).random(count))
