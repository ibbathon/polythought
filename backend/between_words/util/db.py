import os

from between_words.db_models import db


def init_pony(**bind_params):
    if db.provider_name is not None:
        return

    create_tables = bind_params.pop("create_tables", False)
    if not bind_params:
        bind_params = {
            "provider": "postgres",
            "dsn": os.getenv("DB_DSN"),
        }
    db.bind(**bind_params)
    db.generate_mapping(create_tables=create_tables)
