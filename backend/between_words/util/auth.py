import os
from datetime import datetime, timedelta, timezone
from typing import Annotated
from uuid import uuid4

from fastapi import Depends, HTTPException
from fastapi.security import OAuth2PasswordBearer
from jose import JWTError, jwt
from pony import orm

from between_words.api_models import UserResponse
from between_words.db_models import Session, User

# TODO: this module definitely feels like it needs a refactoring
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="/api/auth/login")
ALGORITHM = "HS256"


def validate_credentials(username: str, password: str) -> User | None:
    with orm.db_session:
        user = User.get(username=username)
    if user is None:
        return None
    password_is_correct = user.validate_password(password)
    if not password_is_correct:
        return None
    return user


def create_access_token(user: User):
    expires_delta = timedelta(
        minutes=float(os.getenv("ACCESS_TOKEN_EXPIRE_MINUTES", 15))
    )
    expiry_date = datetime.now(timezone.utc) + expires_delta
    # salt is purely to prevent token intersections, so we can be assured that
    # rows in the sessions table will be unique
    token_salt = str(uuid4())
    token = jwt.encode(
        {"sub": user.username, "exp": expiry_date, "salt": token_salt},
        os.getenv("JWT_SECRET"),
        algorithm=ALGORITHM,
    )

    # now that we have the token, store it as a session
    with orm.db_session:
        user = User.get(username=user.username)
        Session(user=user, token=token)

    return token


def decode_access_token(token: str):
    credentials_exception = HTTPException(
        status_code=401,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )

    # pull apart the jwt
    try:
        payload = jwt.decode(token, os.getenv("JWT_SECRET"), algorithms=[ALGORITHM])
    except JWTError:
        # NOTE: jwt.decode automatically handles checking expiry date, so we
        # don't need a later if branch to handle that
        raise credentials_exception
    username: str = payload.get("sub")
    expiry_timestamp: int = payload.get("exp")
    if username is None or expiry_timestamp is None:
        raise credentials_exception

    # get the data from DB
    with orm.db_session:
        user = User.get(username=username)
        if user is None:
            raise credentials_exception
        session = Session.get(user=user, token=token)
        if session is None or session.valid is False:
            raise credentials_exception

    return user


async def get_current_user(token: Annotated[str, Depends(oauth2_scheme)]):
    dbuser = decode_access_token(token)
    user = UserResponse.model_validate(dbuser)
    return user
