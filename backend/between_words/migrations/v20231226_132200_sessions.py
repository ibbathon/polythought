from ponyherd.migration import PonyHerdMigration


class CreateSessions(PonyHerdMigration):
    def up(self):
        self._create_table(
            "session",
            ("uuid", "UUID", "primary key not null"),
            ("created_at", "timestamp", "not null"),
            ("updated_at", "timestamp", ""),
            ("user", "UUID", 'not null references "user" ("uuid") on delete cascade'),
            ("token", "text", "not null"),
            ("valid", "boolean", "not null"),
        )
        self._add_index("session", "user", "session_user")

    def down(self):
        self._drop_table("session")
