import re
from pathlib import Path

from ponyherd.migration import PonyHerdMigration


class AddWordTableAndPopulate(PonyHerdMigration):
    def up(self):
        self._create_table(
            "word",
            ("word", "text", "not null"),
            ("dictionary", "text", "not null"),
            constraints=['primary key ("word", "dictionary")'],
        )

        # sqlite's prepares are different from postgres's, and I'm not even
        # testing data migrations, so skip this
        if self._db.provider_name == "sqlite":
            return
        else:  # pragma: no cover
            self._db.execute(
                """
                prepare wordplan (text, text) as
                insert into word (word, dictionary) values($$1, $$2)
                """
            )
            wordsfile = Path(__file__).parent / "default.words"
            with wordsfile.open("r") as f:
                for row in f:
                    # delete comments
                    row = re.sub(r"#.*", "", row).strip()
                    # if the line was solely a comment, skip it
                    if not row:
                        continue

                    self._db.execute(
                        """
                        execute wordplan($row, 'default')
                        """
                    )

    def down(self):
        self._drop_table("word")
