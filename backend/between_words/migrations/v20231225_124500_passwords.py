from ponyherd.migration import PonyHerdMigration


class Passwords(PonyHerdMigration):
    def up(self):
        self._add_column(
            "user", ("password_salt", "text", "not null default 'invalid'")
        )
        self._add_index("user", "username", unique=True)

    def down(self):
        self._drop_column("user", "password_salt")
        self._drop_index("user", "username", unique=True)
