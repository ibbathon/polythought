from ponyherd.migration import PonyHerdMigration


class InitialDb(PonyHerdMigration):
    def up(self):
        self._create_table(
            "user",
            ("uuid", "UUID", "primary key not null"),
            ("created_at", "timestamp", "not null"),
            ("updated_at", "timestamp", ""),
            ("username", "text", "not null"),
            ("password_hash", "text", "not null"),
            ("favorite_fruit", "text", "not null"),
        )

    def down(self):
        self._drop_table("user")
