#!/bin/bash
# TODO: figure out how to get this into a common location

COMMAND=$@
echo "$COMMAND"

# docker has trouble passing sigterm to subprocess, so we have to do some tricky
# nonsense to pass it along
handle_term() {
  kill -SIGTERM "$pid"
  wait "$pid"
  exit 143
}
trap 'kill ${!}; handle_term' SIGTERM

# the actual app
exec "$COMMAND" &
pid="$!"

while true; do
  tail -f /dev/null &
  wait ${!}
done
