MODULE = "between_words.routes.words"


class TestWordsRoutes:
    def test_random_with_defaults(self, test_client, valid_auth_headers, mocker):
        mock_random_words = mocker.patch(
            f"{MODULE}.random_words", return_value=["some", "words"]
        )

        response = test_client.get("/api/words/random", headers=valid_auth_headers)

        assert response.status_code == 200
        assert response.json() == ["some", "words"]
        mock_random_words.assert_called_once_with(1, "default")

    def test_random_with_custom(self, test_client, valid_auth_headers, mocker):
        mock_random_words = mocker.patch(
            f"{MODULE}.random_words", return_value=["some", "words"]
        )

        response = test_client.get(
            "/api/words/random?count=10&dictionary=other", headers=valid_auth_headers
        )

        assert response.status_code == 200
        assert response.json() == ["some", "words"]
        mock_random_words.assert_called_once_with(10, "other")

    def test_random_with_invalid_count(self, test_client, valid_auth_headers, mocker):
        mock_random_words = mocker.patch(f"{MODULE}.random_words")

        response = test_client.get(
            "/api/words/random?count=0", headers=valid_auth_headers
        )

        assert response.status_code == 422
        assert response.json() == {"detail": "count must be a positive integer"}
        mock_random_words.assert_not_called()

    def test_random_with_invalid_dictionary(
        self, test_client, valid_auth_headers, mocker
    ):
        mock_random_words = mocker.patch(f"{MODULE}.random_words", return_value=[])

        response = test_client.get(
            "/api/words/random?dictionary=invalid", headers=valid_auth_headers
        )

        assert response.status_code == 422
        assert response.json() == {"detail": "invalid dictionary"}
        mock_random_words.assert_called_once_with(1, "invalid")
