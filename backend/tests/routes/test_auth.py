from pony import orm

from between_words.db_models import Session, User


class TestAuthRoutes:
    def test_login_valid(self, test_client, auth_user):
        response = test_client.post(
            "/api/auth/login", data={"username": "authuser", "password": "testpw"}
        )

        assert response.status_code == 200
        response_data = response.json()
        assert response_data["token_type"] == "bearer"
        access_token = response_data["access_token"]

        with orm.db_session:
            session = Session.get(token=access_token)
            authuser = User.get(username=auth_user.username)
            assert session is not None
            assert session.user == authuser

    def test_login_invalid(self, test_client, auth_user):
        response = test_client.post(
            "/api/auth/login", data={"username": "authuser", "password": "invalid"}
        )
        assert response.status_code == 400
        assert response.json() == {"detail": "Incorrect username or password"}

        response = test_client.post(
            "/api/auth/login", data={"username": "invalid", "password": "invalid"}
        )
        assert response.status_code == 400
        assert response.json() == {"detail": "Incorrect username or password"}

        assert response.status_code == 400

    def test_logout_valid(self, test_client, valid_auth_headers):
        with orm.db_session:
            authuser = User.get(username="authuser")
            session = Session.get(user=authuser)
            token = session.token

        response = test_client.post("/api/auth/logout", headers=valid_auth_headers)

        assert response.status_code == 200

        with orm.db_session:
            session = Session.get(token=token)
            assert session.valid is False

    def test_logout_invalid(self, test_client, auth_token):
        response = test_client.post(
            "/api/auth/logout", headers={"Authorization": "Bearer invalid"}
        )

        assert response.status_code == 401
        assert response.json() == {"detail": "Session is already invalidated"}

        with orm.db_session:
            session = Session.get(token=auth_token)
            assert session.valid is True
