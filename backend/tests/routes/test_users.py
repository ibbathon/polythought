from freezegun import freeze_time
from pony import orm

from between_words.db_models import User

MODULE = "between_words.routes.users"


class TestUsersRoutes:
    @freeze_time("2023-09-02")
    def test_all_users(self, test_client, valid_auth_headers):
        with orm.db_session:
            user2 = User(username="test2")
            user1 = User(username="test1")
            authuser = User.get(username="authuser")

        response = test_client.get("/api/users", headers=valid_auth_headers)

        assert response.status_code == 200
        assert sorted(response.json(), key=lambda e: e["username"]) == [
            {
                "uuid": str(authuser.uuid),
                "created_at": authuser.created_at.isoformat(),
                "updated_at": authuser.updated_at.isoformat(),
                "username": "authuser",
            },
            {
                "uuid": str(user1.uuid),
                "created_at": "2023-09-02T00:00:00Z",
                "updated_at": "2023-09-02T00:00:00Z",
                "username": "test1",
            },
            {
                "uuid": str(user2.uuid),
                "created_at": "2023-09-02T00:00:00Z",
                "updated_at": "2023-09-02T00:00:00Z",
                "username": "test2",
            },
        ]

    @freeze_time("2023-09-03")
    def test_get_other_user(self, test_client, valid_auth_headers):
        with orm.db_session:
            getuser = User(username="test")

        response = test_client.get("/api/users/test", headers=valid_auth_headers)

        assert response.status_code == 200
        assert response.json() == {
            "uuid": str(getuser.uuid),
            "created_at": "2023-09-03T00:00:00Z",
            "updated_at": "2023-09-03T00:00:00Z",
            "username": "test",
        }

    def test_get_self_user(self, test_client, valid_auth_headers):
        with orm.db_session:
            User(username="test")
            authuser = User.get(username="authuser")

        response = test_client.get("/api/users/authuser", headers=valid_auth_headers)

        assert response.status_code == 200
        assert response.json() == {
            "uuid": str(authuser.uuid),
            "created_at": authuser.created_at.isoformat(),
            "updated_at": authuser.updated_at.isoformat(),
            "username": "authuser",
            "favorite_fruit": "lemon",
        }
