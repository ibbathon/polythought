from pony import orm

from between_words.db_models import User
from tests.helpers import DBModelTester


class TestUserModel(DBModelTester):
    _db_model = User
    _all_attributes = {
        "username": "test1",
        "password": "test2",
        "favorite_fruit": "test3",
    }
    _expected_attributes = {
        "username": "test1",
        "favorite_fruit": "test3",
    }
    _required_attributes = [
        "username",
    ]
    _default_values: dict = {
        "favorite_fruit": "lemon",
    }
    _moddable_attr = "username"

    def test_password(self):
        with orm.db_session:
            user = self._db_model(**self._all_attributes)
        assert user.password == "***"
        assert user.validate_password("test2") is True

        with orm.db_session:
            user = User.get(username="test1")
            user.password = "newpw"
        assert user.validate_password("test2") is False
        assert user.validate_password("newpw") is True
