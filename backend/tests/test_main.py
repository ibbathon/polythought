from fastapi.testclient import TestClient

from between_words.main import app

MODULE = "between_words.main"


class TestSetup:
    def test_lifespan(self, mocker):
        mock_init_pony = mocker.patch(f"{MODULE}.init_pony")
        with TestClient(app):
            mock_init_pony.assert_called_once_with()
        mock_init_pony.assert_called_once_with()
