import pytest
from pony import orm

from between_words.db_models import Word
from between_words.util.word import random_words

MODULE = "between_words.util.word"


class TestRandomWords:
    SAMPLE_DEFAULT_WORDS = set(
        (
            """
            Necessitatibus id maiores esse. Alias deserunt expedita nesciunt fuga iusto tempore dolores odit. Dignissimos nam consectetur ut officiis quia ut officia repellat.
            """  # noqa: E501
        )
        .strip()
        .lower()
        .replace(".", "")
        .split(" ")
    )
    SAMPLE_OTHER_WORDS = ["some", "other", "words"]

    @pytest.fixture(autouse=True)
    def populate_default_words(self, db):
        with orm.db_session:
            for word in self.SAMPLE_DEFAULT_WORDS:
                Word(word=word, dictionary="default")

    @pytest.fixture
    def populate_other_words(self, db):
        with orm.db_session:
            for word in self.SAMPLE_OTHER_WORDS:
                Word(word=word, dictionary="other")

    def test_defaults(self):
        with orm.db_session:
            response = random_words()

        assert len(response) == 1
        assert response[0] in self.SAMPLE_DEFAULT_WORDS

    def test_large_count(self):
        with orm.db_session:
            response = random_words(20)

        assert len(response) == 20
        assert len(set(response)) == 20
        assert set(response) <= self.SAMPLE_DEFAULT_WORDS

    def test_count_greater_than_table(self):
        with orm.db_session:
            response = random_words(50)

        assert len(response) == 21
        assert len(set(response)) == 21
        assert set(response) == self.SAMPLE_DEFAULT_WORDS

    def test_other_dictionary(self, populate_other_words):
        with orm.db_session:
            response = random_words(3, "other")
        assert list(sorted(response)) == ["other", "some", "words"]

    def test_missing_dictionary(self):
        with orm.db_session:
            response = random_words(1, "invalid")
        assert response == []
