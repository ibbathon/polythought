import os

import pytest

from between_words.util.db import init_pony

MODULE = "between_words.util.db"


class TestInitPony:
    @pytest.fixture(autouse=True)
    def mock_db(self, mocker):
        mock_db = mocker.patch(f"{MODULE}.db", provider_name=None)

        def mock_bind(*args, **kwargs):
            mock_db.provider_name = "set"

        mock_db.bind = mocker.Mock(wraps=mock_bind)
        return mock_db

    def test_default_params(self, mocker, mock_db):
        mocker.patch.dict(
            os.environ,
            {
                "DB_DSN": "mock_dsn",
            },
            clear=True,
        )
        init_pony()
        mock_db.bind.assert_called_once_with(
            provider="postgres",
            dsn="mock_dsn",
        )
        mock_db.generate_mapping.assert_called_once_with(create_tables=False)

    def test_multiple_calls_binds_once(self, mock_db):
        init_pony()
        init_pony()
        mock_db.bind.assert_called_once()
        mock_db.generate_mapping.assert_called_once()

    def test_custom_params(self, mock_db):
        init_pony(first="test", second="test2")
        mock_db.bind.assert_called_once_with(first="test", second="test2")
        mock_db.generate_mapping.assert_called_once_with(create_tables=False)

    def test_create_tables_override(self, mock_db):
        init_pony(create_tables=False)
        mock_db.generate_mapping.assert_called_once_with(create_tables=False)
