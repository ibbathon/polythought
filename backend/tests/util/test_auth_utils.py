import os
from datetime import datetime, timedelta, timezone

import pytest
from fastapi import HTTPException
from jose import JWTError, jwt
from pony import orm

from between_words.db_models import Session
from between_words.util.auth import ALGORITHM, decode_access_token

MODULE = "between_words.util.auth"


class TestTokenHandling:
    def test_decode_jwt_failure(self, mocker):
        mocker.patch(f"{MODULE}.jwt", **{"decode.side_effect": JWTError})

        with pytest.raises(HTTPException) as exc_info:
            decode_access_token("invalid")
        assert exc_info.value.detail == "Could not validate credentials"

    def test_decode_missing_username(self, mocker):
        exp = datetime.now(timezone.utc) + timedelta(minutes=5)
        token = jwt.encode({"exp": exp}, os.getenv("JWT_SECRET"), algorithm=ALGORITHM)

        with pytest.raises(HTTPException) as exc_info:
            decode_access_token(token)
        assert exc_info.value.detail == "Could not validate credentials"

    def test_decode_missing_expiry_date(self, mocker, auth_user):
        token = jwt.encode(
            {"sub": auth_user.username},
            os.getenv("JWT_SECRET"),
            algorithm=ALGORITHM,
        )

        with pytest.raises(HTTPException) as exc_info:
            decode_access_token(token)
        assert exc_info.value.detail == "Could not validate credentials"

    def test_decode_expired_token(self, mocker, auth_user):
        exp = datetime.now(timezone.utc) - timedelta(minutes=5)
        token = jwt.encode(
            {"sub": auth_user.username, "exp": exp},
            os.getenv("JWT_SECRET"),
            algorithm=ALGORITHM,
        )

        with pytest.raises(HTTPException) as exc_info:
            decode_access_token(token)
        assert exc_info.value.detail == "Could not validate credentials"

    def test_decode_missing_user(self, mocker):
        exp = datetime.now(timezone.utc) + timedelta(minutes=5)
        token = jwt.encode(
            {"sub": "other_user", "exp": exp},
            os.getenv("JWT_SECRET"),
            algorithm=ALGORITHM,
        )

        with pytest.raises(HTTPException) as exc_info:
            decode_access_token(token)
        assert exc_info.value.detail == "Could not validate credentials"

    def test_decode_missing_session(self, mocker, auth_user):
        exp = datetime.now(timezone.utc) + timedelta(minutes=5)
        token = jwt.encode(
            {"sub": auth_user.username, "exp": exp},
            os.getenv("JWT_SECRET"),
            algorithm=ALGORITHM,
        )

        with pytest.raises(HTTPException) as exc_info:
            decode_access_token(token)
        assert exc_info.value.detail == "Could not validate credentials"

    def test_decode_invalidated_session(self, mocker, auth_token):
        with orm.db_session:
            session = Session.get(token=auth_token)
            session.valid = False

        with pytest.raises(HTTPException) as exc_info:
            decode_access_token(auth_token)
        assert exc_info.value.detail == "Could not validate credentials"
