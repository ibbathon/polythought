import pytest
from pony import orm

from ponyherd.migration import InvalidMigrationError, PonyHerdMigration

MODULE = "ponyherd.migration"


class TestPonyHerdMigration:
    @pytest.fixture
    def mock_logging(self, mocker):
        mock_logging = mocker.patch(f"{MODULE}.logging")
        return mock_logging

    @pytest.fixture(autouse=True)
    def mock_logger(self, mock_logging):
        return mock_logging.getLogger.return_value

    @pytest.fixture
    def mock_db(self, mocker):
        return mocker.MagicMock(orm.Database, provider_name="sqlite")

    def test_init_uses_correct_logger(self, mock_db, mock_logging):
        PonyHerdMigration(mock_db)
        mock_logging.getLogger.assert_called_once_with("PonyHerd")

    def test_init_with_indent_sets_indent_for_all_logs(self, mock_db, mock_logger):
        PonyHerdMigration(mock_db, 0)._log("test_text")
        mock_logger.info.assert_called_once_with("test_text")

    def test_up_is_unimplemented(self, mock_db):
        with pytest.raises(NotImplementedError):
            PonyHerdMigration(mock_db).up()

    def test_down_is_unimplemented(self, mock_db):
        with pytest.raises(NotImplementedError):
            PonyHerdMigration(mock_db).down()

    def test_create_table_minimal(self, mock_db, mock_logger):
        PonyHerdMigration(mock_db)._create_table(
            "test_table",
            ("col1", "uuid", "not null"),
        )

        expected_command = 'create table  "test_table" ("col1" uuid not null)'
        mock_db.execute.assert_called_once_with(expected_command)
        mock_logger.info.assert_called_once_with("    creating table test_table")
        mock_logger.debug.assert_called_once_with(f"    executing {expected_command}")

    def test_create_table_all_params(self, mock_db, mock_logger):
        PonyHerdMigration(mock_db)._create_table(
            "another_test",
            ("col1", "uuid", "not null"),
            ("other_col", "timestamp", ""),
            constraints=("some constraint", "another constraint"),
            if_not_exists=True,
        )

        expected_command = (
            'create table if not exists "another_test" ('
            + '"col1" uuid not null, '
            + '"other_col" datetime , '
            + "some constraint, another constraint"
            + ")"
        )
        mock_db.execute.assert_called_once_with(expected_command)
        mock_logger.info.assert_called_once_with("    creating table another_test")
        mock_logger.debug.assert_called_once_with(f"    executing {expected_command}")

    def test_create_table_without_columns_fails(self, mock_db, mock_logger):
        with pytest.raises(InvalidMigrationError):
            PonyHerdMigration(mock_db)._create_table("test_table")
        mock_db.execute.assert_not_called()

    def test_create_table_with_invalid_columns_fails(self, mock_db):
        with pytest.raises(InvalidMigrationError):
            PonyHerdMigration(mock_db)._create_table("test_table", ("c1",))
        mock_db.execute.assert_not_called()

    def test_drop_table(self, mock_db, mock_logger):
        PonyHerdMigration(mock_db)._drop_table("test_table")
        expected_command = 'drop table if exists "test_table"'
        mock_db.execute.assert_called_once_with(expected_command)
        mock_logger.info.assert_called_once_with("    dropping table test_table")
        mock_logger.debug.assert_called_once_with(f"    executing {expected_command}")

    def test_postgres_provider_translations(self, mock_db):
        mock_db.provider_name = "postgres"
        PonyHerdMigration(mock_db)._create_table(
            "translation_test",
            ("col1", "timestamp", "not null default 'something'"),
        )

        expected_command = (
            'create table  "translation_test" '
            + "(\"col1\" timestamp not null default 'something')"
        )
        mock_db.execute.assert_called_once_with(expected_command)

    def test_add_column(self, mock_db, mock_logger):
        PonyHerdMigration(mock_db)._add_column(
            "test_table",
            ("newcol", "int", "not null default 'something'"),
        )

        expected_command = (
            'alter table "test_table" '
            + 'add column "newcol" int not null default something'
        )
        mock_db.execute.assert_called_once_with(expected_command)
        mock_logger.info.assert_called_once_with(
            "    adding column newcol to table test_table"
        )
        mock_logger.debug.assert_called_once_with(f"    executing {expected_command}")

    def test_add_column_with_invalid_column(self, mock_db, mock_logger):
        with pytest.raises(InvalidMigrationError):
            PonyHerdMigration(mock_db)._add_column("test_table", ("12",))
        mock_db.execute.assert_not_called()

    def test_drop_column(self, mock_db, mock_logger):
        PonyHerdMigration(mock_db)._drop_column("test_table", "delcol")

        expected_command = 'alter table "test_table" drop column "delcol"'
        mock_db.execute.assert_called_once_with(expected_command)
        mock_logger.info.assert_called_once_with(
            "    dropping column delcol from table test_table"
        )
        mock_logger.debug.assert_called_once_with(f"    executing {expected_command}")

    @pytest.mark.parametrize("provider", ["sqlite", "postgres"])
    @pytest.mark.parametrize("unique", [True, False])
    @pytest.mark.parametrize("index_name", [None, "fancy_index"])
    def test_add_index(
        self,
        mocker,
        mock_db,
        mock_logger,
        provider,
        unique,
        index_name,
    ):
        mock_db.provider_name = provider

        if unique:
            PonyHerdMigration(mock_db)._add_index(
                "test_table",
                "idxcol",
                index=index_name,
                unique=True,
            )
        else:
            PonyHerdMigration(mock_db)._add_index(
                "test_table",
                "idxcol",
                index=index_name,
            )

        expected_name = "test_table_idxcol_key" if index_name is None else index_name
        expected_commands = [
            f'create {"unique " if unique else ""}index "{expected_name}" '
            + 'on "test_table" ("idxcol")',
        ]
        expected_logs = [
            mocker.call(f"    adding unique index {expected_name}"),
        ]
        if provider == "postgres" and unique:
            expected_commands.append(
                f'alter table "test_table" add constraint "{expected_name}" '
                + f'unique using index "{expected_name}"',
            )
            expected_logs.append(
                mocker.call(f"    adding unique constraint {expected_name}"),
            )

        assert mock_db.execute.call_args_list == list(
            mocker.call(c) for c in expected_commands
        )
        mock_logger.info.call_args_list == expected_logs
        mock_logger.debug.call_args_list == list(
            mocker.call(f"    executing {c}") for c in expected_commands
        )

    @pytest.mark.parametrize("provider", ["sqlite", "postgres"])
    @pytest.mark.parametrize("unique", [True, False])
    @pytest.mark.parametrize("index_name", [None, "fancy_index"])
    def test_drop_index(
        self,
        mocker,
        mock_db,
        mock_logger,
        provider,
        unique,
        index_name,
    ):
        mock_db.provider_name = provider

        if unique:
            PonyHerdMigration(mock_db)._drop_index(
                "test_table",
                "idxcol",
                index=index_name,
                unique=True,
            )
        else:
            PonyHerdMigration(mock_db)._drop_index(
                "test_table",
                "idxcol",
                index=index_name,
            )

        expected_name = "test_table_idxcol_key" if index_name is None else index_name
        if provider == "postgres" and unique:
            expected_command = (
                f'alter table "test_table" drop constraint "{expected_name}"'
            )
            expected_log = f"    dropping unique constraint {expected_name}"
        else:
            expected_command = f'drop index "{expected_name}"'
            expected_log = (
                f'    dropping {"unique " if unique else ""}index {expected_name}'
            )

        mock_db.execute.assert_called_once_with(expected_command)
        mock_logger.info.assert_called_once_with(expected_log)
        mock_logger.debug.assert_called_once_with(f"    executing {expected_command}")
