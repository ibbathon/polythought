import pytest
from freezegun import freeze_time

from ponyherd.herder import Herder, InvalidCommandError
from ponyherd.migration import InvalidMigrationError

MODULE = "ponyherd.herder"
MIGMODULE = "ponyherd.migration"


class TestHerder:
    MIG_CONFIG = [
        {"name": "v1_mig", "class": "V1Mig", "applied": True},
        {"name": "v2_mig", "class": "V2Mig", "applied": False},
        {"name": "v3_mig", "class": "V3Mig", "applied": True},
        {"name": "v4_mig", "class": "V4Mig", "applied": False},
    ]

    @pytest.fixture
    def mock_db(self, mocker):
        def mock_execute(exec_str, params=None):
            if "select applied" not in exec_str:
                return
            for config in self.MIG_CONFIG:
                if params["version"] == config["name"]:
                    return mocker.Mock(
                        fetchone=mocker.Mock(return_value=[bool(config["applied"])])
                    )

        mock_db = mocker.Mock(
            execute=mocker.Mock(wraps=mock_execute),
            provider_name=None,
        )
        return mock_db

    @pytest.fixture(autouse=True)
    def mock_config(self, tmp_path, mocker, mock_db):
        mock = mocker.Mock(
            ponydb=mock_db,
            migrations_path=tmp_path,
            db_provider="testing_db_provider",
            db_dsn="testing_db_dsn_envvar",
            log_level="DEBUG",
        )
        mocker.patch(f"{MODULE}.Config", return_value=mock)
        return mock

    @pytest.fixture(autouse=True)
    def mock_env(self, mocker):
        mocker.patch.dict(
            f"{MODULE}.os.environ",
            {"testing_db_dsn_envvar": "testing_db_dsn"},
        )

    @pytest.fixture
    def mock_logging(self, mocker):
        return mocker.patch(f"{MODULE}.logging")

    @pytest.fixture
    def mock_logger(self, mock_logging):
        return mock_logging.getLogger.return_value

    @pytest.fixture
    def mock_mig_logging(self, mocker):
        return mocker.patch(f"{MIGMODULE}.logging")

    @pytest.fixture(autouse=True)
    def mock_migrations(self, tmp_path):
        for config in self.MIG_CONFIG:
            with (tmp_path / f"{config['name']}.py").open("w") as f:
                f.write(
                    f"""
from ponyherd.migration import PonyHerdMigration
class {config['class']}(PonyHerdMigration):
    def up(self):
        self._db.{config['name']}_up()
    def down(self):
        self._db.{config['name']}_down()
                    """.strip()
                )

    def test_init_happy_path(
        self,
        mock_db,
        mock_logging,
        mock_logger,
        mock_config,
        mock_mig_logging,
    ):
        herder = Herder()
        assert herder._config == mock_config
        assert herder._logger == mock_logger
        assert herder._db == mock_db
        assert set(herder._migrations.keys()) == {
            "v1_mig",
            "v2_mig",
            "v3_mig",
            "v4_mig",
        }

        mock_logging.getLogger.assert_called_once_with("PonyHerd")
        mock_mig_logging.getLogger.assert_called_once_with("PonyHerd")
        mock_logger.setLevel.assert_called_once_with("DEBUG")
        mock_db.bind.assert_called_once_with(
            provider="testing_db_provider", dsn="testing_db_dsn"
        )
        mock_db.execute.assert_called_once_with(
            'create table if not exists "ponyherd_versions" '
            + '("version" varchar primary key, "applied" boolean default false)'
        )

    def test_init_already_bound(self, mock_db):
        mock_db.provider_name = "something"
        Herder()
        mock_db.bind.assert_not_called()

    def test_unreadable_mig_file(self, mocker, tmp_path):
        mock_importlib = mocker.patch(
            f"{MODULE}.importlib",
            **{"util.spec_from_file_location.return_value": None},
        )
        with pytest.raises(
            InvalidMigrationError,
            match=f"unreadable migration file {tmp_path}/v1_mig.py",
        ):
            Herder()
        mock_importlib.util.module_from_spec.assert_not_called()

        mock_importlib = mocker.patch(
            f"{MODULE}.importlib",
            **{"util.spec_from_file_location.return_value": mocker.Mock(loader=None)},
        )
        with pytest.raises(
            InvalidMigrationError,
            match=f"unreadable migration file {tmp_path}/v1_mig.py",
        ):
            Herder()
        mock_importlib.util.module_from_spec.assert_not_called()

    def test_multiple_migrations_in_mig_file(self, tmp_path):
        with (tmp_path / "invalid_mig.py").open("w") as f:
            f.write(
                """
from ponyherd.migration import PonyHerdMigration
class InvalidMig1(PonyHerdMigration): pass
class InvalidMig2(PonyHerdMigration): pass
                """.strip()
            )
        with pytest.raises(
            InvalidMigrationError,
            match=(
                f"migration file {tmp_path}/invalid_mig.py has more than one"
                + " migration"
            ),
        ):
            Herder()

    def _assert_version_insert_calls(self, mock_db, value, *versions):
        # verify that ponyherd_versions rows were updated
        assert_count = 0
        for call in mock_db.execute.call_args_list:
            pos_params = call[0]
            # skip the _ensure_versions_table call
            if "create table" in pos_params[0]:
                continue
            # skip the _is_applied calls
            if "select applied" in pos_params[0]:
                continue
            assert "insert into ponyherd_versions" in pos_params[0]
            assert pos_params[1] == {
                "version": versions[assert_count],
                "status": value,
            }
            assert_count += 1
        assert assert_count == len(versions), "missing version insert calls"

    def test_execute_default(self, mock_logger, mock_db, mocker):
        Herder().execute()
        mock_db.v1_mig_up.assert_not_called()
        mock_db.v2_mig_up.assert_called_once_with()
        mock_db.v3_mig_up.assert_not_called()
        mock_db.v4_mig_up.assert_called_once_with()
        assert mock_logger.info.call_args_list == [
            mocker.call("applying v2_mig V2Mig"),
            mocker.call("applying v4_mig V4Mig"),
        ]
        self._assert_version_insert_calls(mock_db, True, "v2_mig", "v4_mig")

    def test_execute_down(self, mock_logger, mock_db, mocker):
        Herder().execute("down")
        mock_db.v1_mig_down.assert_called_once_with()
        mock_db.v2_mig_down.assert_not_called()
        mock_db.v3_mig_down.assert_called_once_with()
        mock_db.v4_mig_down.assert_not_called()
        assert mock_logger.info.call_args_list == [
            mocker.call("reverting v3_mig V3Mig"),
            mocker.call("reverting v1_mig V1Mig"),
        ]
        self._assert_version_insert_calls(mock_db, False, "v3_mig", "v1_mig")

    def test_execute_downone(self, mock_logger, mock_db):
        Herder().execute("downone")
        mock_db.v1_mig_down.assert_not_called()
        mock_db.v2_mig_down.assert_not_called()
        mock_db.v3_mig_down.assert_called_once_with()
        mock_db.v4_mig_down.assert_not_called()
        mock_logger.info.assert_called_once_with("reverting v3_mig V3Mig")
        self._assert_version_insert_calls(mock_db, False, "v3_mig")

    @freeze_time("2024-02-17 10:53:07")
    def test_execute_add(self, tmp_path):
        Herder().execute("add", "Someone's siLLY-Tastic name")

        filepath = tmp_path / "v20240217_105307_someone_s_silly_tastic_name.py"
        assert filepath in list(tmp_path.glob("*.py"))

        with filepath.open("r") as f:
            contents = f.read()
        assert (
            contents
            == """
from ponyherd.migration import PonyHerdMigration
class SomeonesSillytasticName(PonyHerdMigration):
    def up(self): pass
    def down(self): pass
        """.strip()
        )

    def test_execute_invalid(self):
        with pytest.raises(InvalidCommandError) as exc_info:
            Herder().execute("invalid")
        assert str(exc_info.value) == "unrecognized command invalid"

    def test_apply_migration_on_applied(self, mock_db):
        herder = Herder()

        # unforced
        with pytest.raises(InvalidCommandError, match="migration is already applied"):
            herder.apply_migration("v1_mig")
        mock_db.v1_mig_up.assert_not_called()

        # forced
        herder.apply_migration("v1_mig", force=True)
        mock_db.v1_mig_up.assert_called_once_with()

    def test_revert_migration_on_unapplied(self, mock_db):
        herder = Herder()

        # unforced
        with pytest.raises(InvalidCommandError, match="migration is not yet applied"):
            herder.revert_migration("v2_mig")
        mock_db.v2_mig_down.assert_not_called()

        # forced
        herder.revert_migration("v2_mig", force=True)
        mock_db.v2_mig_down.assert_called_once_with()

    def test_get_versions(self):
        herder = Herder()
        assert herder.get_versions() == ("v1_mig", "v2_mig", "v3_mig", "v4_mig")
