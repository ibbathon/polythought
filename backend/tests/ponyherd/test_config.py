from pathlib import Path
from typing import Optional

import pytest
from pony import orm

from ponyherd.config import Config, InvalidConfig
from ponyherd.util import PyprojectNotFound

MODULE = "ponyherd.config"
UTILMODULE = "ponyherd.util"


class TestConfig:
    TESTING_CONFIG_VALUES = {
        "ponydb": "a_module.a_submod:an_attr.a_subattr",
        "migrations": "a_module.another_submod",
        "db-provider": "a_provider",
        "db-dsn-env-var": "AN_ENVVAR",
    }

    def _write_pyproject(
        self,
        write_path: Path,
        section: str = "[tool.ponyherd]",
        values: Optional[dict[str, str]] = None,
        use_defaults: Optional[list[str]] = None,
    ):
        values_to_write: dict[str, str] = {}
        if use_defaults is None:
            values_to_write = dict(self.TESTING_CONFIG_VALUES)
        else:
            for k in use_defaults:
                values_to_write[k] = self.TESTING_CONFIG_VALUES[k]
        if values is not None:
            values_to_write.update(values)

        with write_path.open("w") as f:
            f.write(section + "\n")
            for k, v in values_to_write.items():
                f.write(f"{k} = '{v}'\n")

    @pytest.fixture(autouse=True)
    def create_basic_pyproject(self, tmp_path):
        self._write_pyproject(tmp_path / "pyproject.toml")

    @pytest.fixture(autouse=True)
    def patch_cwd(self, mocker, tmp_path):
        mocker.patch(
            f"{UTILMODULE}.Path",
            **{"cwd.return_value.resolve.return_value": tmp_path},
        )

    @pytest.fixture(autouse=True)
    def mock_config_importlib(self, mocker):
        return mocker.patch(
            f"{MODULE}.importlib",
            **{"import_module.return_value.__path__": ["test_path"]},
        )

    @pytest.fixture(autouse=True)
    def mock_util_importlib(self, mocker):
        mock = mocker.patch(f"{UTILMODULE}.importlib")
        mock.import_module.return_value.an_attr.a_subattr.__class__ = orm.Database
        return mock

    def test_deep_path_with_false_pyprojects(
        self, tmp_path, mocker, mock_util_importlib
    ):
        subdir = tmp_path / "sub"
        subdir.mkdir()
        emptydir = subdir / "empty"
        emptydir.mkdir()
        startdir = emptydir / "start"
        startdir.mkdir()
        # invalid-toml at deepest dir
        with (startdir / "pyproject.toml").open("w") as f:
            f.write("invalid")
        # valid-toml without [tool.ponyherd] section
        with (subdir / "pyproject.toml").open("w") as f:
            f.write("[tool.other]\nword = 5\n")
        mocker.patch(
            f"{UTILMODULE}.Path",
            **{"cwd.return_value.resolve.return_value": startdir},
        )

        config = Config()

        assert (
            config.ponydb
            == mock_util_importlib.import_module.return_value.an_attr.a_subattr
        )
        assert config.migrations_path == Path("test_path")
        assert config.db_provider == "a_provider"
        assert config.db_dsn == "AN_ENVVAR"
        assert config.log_level == "INFO"

    def test_missing_pyproject(self, tmp_path):
        (tmp_path / "pyproject.toml").unlink()
        with pytest.raises(PyprojectNotFound):
            Config()

    def test_invalid_ponydb_paths(self, tmp_path, mocker):
        # missing attr
        self._write_pyproject(
            tmp_path / "pyproject.toml",
            values={"ponydb": "module.only"},
        )
        with pytest.raises(InvalidConfig, match="invalid ponydb path"):
            Config()

        # missing module
        self._write_pyproject(
            tmp_path / "pyproject.toml",
            values={"ponydb": ":attr.only"},
        )
        with pytest.raises(InvalidConfig, match="invalid ponydb path"):
            Config()

    def test_unimportable_ponydb_module(self, mock_util_importlib):
        mock_util_importlib.import_module.side_effect = ModuleNotFoundError
        with pytest.raises(InvalidConfig, match="invalid ponydb path"):
            Config()

    def test_unfindable_ponydb_attr(self, mock_util_importlib):
        mock_util_importlib.import_module.return_value = None
        with pytest.raises(InvalidConfig, match="invalid ponydb path"):
            Config()

    def test_ponydb_wrong_type(self, mock_util_importlib):
        mock_util_importlib.import_module.return_value.an_attr.a_subattr.__class__ = (
            type(None)
        )
        with pytest.raises(InvalidConfig, match="invalid ponydb path"):
            Config()

    def test_missing_pyproject_keys(self, tmp_path):
        all_keys = ("ponydb", "migrations", "db-provider", "db-dsn-env-var")
        write_path = tmp_path / "pyproject.toml"

        for missing_key in all_keys:
            self._write_pyproject(
                write_path,
                use_defaults=[k for k in all_keys if k != missing_key],
            )
            with pytest.raises(InvalidConfig, match="missing required config key"):
                Config()

    def test_invalid_migrations_path(self, mock_config_importlib):
        mock_config_importlib.import_module.side_effect = ModuleNotFoundError
        with pytest.raises(InvalidConfig, match="invalid migrations path"):
            Config()

    def test_custom_log_level(self, tmp_path):
        self._write_pyproject(
            tmp_path / "pyproject.toml",
            values={"log-level": "DEBUG"},
        )
        config = Config()
        assert config.log_level == "DEBUG"
