import pytest
from fastapi.testclient import TestClient
from pony import orm

from between_words.db_models import User
from between_words.db_models import db as pony_db
from between_words.main import app
from between_words.util.auth import create_access_token
from between_words.util.db import init_pony

pytest.register_assert_rewrite("tests.helpers")


@pytest.fixture
def db():
    # NOTE: the bind performed by init_pony (db.bind) will persist across all tests
    # NOTE: sharedmemory might interfere with parallel tests, but it is
    #       needed to deal with the async dependencies of FastAPI
    # TODO: maybe look into per-test filenames
    init_pony(provider="sqlite", filename=":sharedmemory:", create_tables=True)
    pony_db.drop_all_tables(with_all_data=True)
    pony_db.create_tables()
    return pony_db


@pytest.fixture
def test_client(db):
    return TestClient(app)


@pytest.fixture
def auth_user(db):
    with orm.db_session:
        user = User.get(username="authuser")
        if user is None:
            user = User(username="authuser", password="testpw")
    return user


@pytest.fixture
def auth_token(auth_user):
    return create_access_token(auth_user)


@pytest.fixture
def valid_auth_headers(auth_token):
    return {"Authorization": f"Bearer {auth_token}"}
