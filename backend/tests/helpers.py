from datetime import datetime, timezone
from typing import Any
from uuid import UUID

import pytest
from freezegun import freeze_time
from pony import orm

from between_words.db_models import db


class DBModelTester:
    _db_model: db.Entity  # type: ignore
    _all_attributes: dict[str, Any]
    _expected_attributes: dict[str, Any]
    _required_attributes: list[str]
    _default_values: dict[str, Any]

    def test_all_attributes(self, db):
        with orm.db_session:
            self._db_model(**self._all_attributes)
        with orm.db_session:
            instance = self._db_model.select().first()

        for attr, expected_value in self._expected_attributes.items():
            assert getattr(instance, attr) == expected_value

    def test_defaulting_attributes(self, db):
        with orm.db_session:
            self._db_model(
                **{
                    a: v
                    for a, v in self._all_attributes.items()
                    if a in self._required_attributes
                }
            )
        with orm.db_session:
            instance = self._db_model.select().first()

        for attr, expected_value in self._expected_attributes.items():
            if attr in self._required_attributes:
                assert getattr(instance, attr) == expected_value
            else:
                assert getattr(instance, attr) == self._default_values[attr]

    def test_required_attributes(self, db):
        for missing_attr in self._required_attributes:
            init_attrs = {
                a: v for a, v in self._all_attributes.items() if a != missing_attr
            }
            with pytest.raises(ValueError) as exc_info:
                with orm.db_session:
                    self._db_model(**init_attrs)
            assert (
                str(exc_info.value)
                == f"Attribute {self._db_model.__name__}.{missing_attr} is required"
            )

            with orm.db_session:
                instance = self._db_model.select().first()
            assert instance is None

    def test_shared_attributes(self, db):
        with freeze_time("2023-09-01"):
            with orm.db_session:
                self._db_model(**self._all_attributes)
        with orm.db_session:
            instance = self._db_model.select().first()

        assert isinstance(instance.uuid, UUID)
        # TODO/CONCERN: SQLite stores/retrieves timestamps as str, while
        # postgres correctly stores/retrieves as datetime
        assert instance.created_at == datetime(
            2023, 9, 1, tzinfo=timezone.utc
        ).isoformat(sep=" ")
        assert instance.updated_at == instance.created_at

        with freeze_time("2023-09-02"):
            with orm.db_session:
                instance = self._db_model.select().first()
                setattr(instance, self._moddable_attr, "modified")
        with orm.db_session:
            instance = self._db_model.select().first()

        assert instance.updated_at == datetime(
            2023, 9, 2, tzinfo=timezone.utc
        ).isoformat(sep=" ")
