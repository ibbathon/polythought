import re

import pytest
from pony import orm

from ponyherd.herder import Herder


class TestMigrations:
    @pytest.fixture
    def mig_db(self, db):
        # because the db state is retained between tests, we need to get rid
        # of the versions table, because we're also dropping the other tables
        with orm.db_session:
            db.execute("drop table if exists ponyherd_versions")
        return db

    def _normalize_str(self, s):
        if not isinstance(s, str):
            return s
        return re.sub(r"\s+", " ", s).strip().lower()

    def _schema_to_dict(self, schema: list[list], type_: str):
        return {
            self._normalize_str(v[1]): self._normalize_str(v[2])
            for v in schema
            if v[0] == type_ and v[2].lower() != "ponyherd_versions"
        }

    def _schema_to_comparable(self, schema: list[list]) -> dict[str, dict]:
        comp = {}
        for t in ("table", "index", "view", "trigger"):
            comp[t] = self._schema_to_dict(schema, t)
        return comp

    def _get_table_columns(
        self, db: orm.Database, table: str
    ) -> dict[str, dict[str, str]]:
        raw_columns = db.execute(f"pragma table_info('{table}')").fetchall()
        columns = {}
        for c in raw_columns:
            columns[self._normalize_str(c[1])] = {
                "type": self._normalize_str(c[2]),
                "notnull": c[3],
                "default": c[4],
                "pk": c[5],
            }
        return columns

    def _get_index_info(
        self, db: orm.Database, index: str, table_columns: dict[str, dict[str, str]]
    ) -> dict[str, dict[str, str]]:
        raw_index = db.execute(f"pragma index_info('{index}')").fetchall()
        data: dict[str, str] = {}
        for row in raw_index:
            rank = row[0]
            column = row[2]  # TODO: handle non-column indexes
            data[rank] = column
        return {"data": data}

    def _get_comparables(
        self, db: orm.Database
    ) -> dict[str, dict[str, dict[str, str]]]:
        with orm.db_session:
            schema = db.execute("select * from sqlite_schema").fetchall()
            schema_comp: dict = {"table": {}, "index": {}, "view": {}, "trigger": {}}
            info: dict[str, dict[str, str]]
            for s in schema:
                type_ = s[0]
                name = s[1]
                table = s[2]
                key = name
                if table == "ponyherd_versions":
                    continue

                if type_ == "table":
                    info = self._get_table_columns(db, name)
                elif type_ == "index":
                    info = self._get_index_info(db, name, schema_comp["table"][table])
                    # indexes get different names when pony creates them, so
                    # normalize to something we know will match
                    key = table + "_" + "_".join(info["data"].values())
                else:
                    # TODO: handle views and triggers
                    info = {}
                schema_comp[type_][key] = info

        return schema_comp

    def test_migrations_match_pony_tables(self, mig_db: orm.Database):
        """test migrations by comparing sqlite_schema to Pony's create_tables

        note that this likely fails to test some aspects of the tables, but is
        "good enough" for now
        (fails to test: whether indexes are unique or just regular;)
        """
        pony_schema = self._get_comparables(mig_db)
        mig_db.drop_all_tables(with_all_data=True)
        Herder().execute()
        mig_schema = self._get_comparables(mig_db)

        assert pony_schema == mig_schema

    def test_last_down_migration(self, mig_db: orm.Database):
        """test the latest migration's down command

        note that we only test the latest to avoid unnecessary computation
        it's assumed that each MR will include at most one migration
        """
        mig_db.drop_all_tables(with_all_data=True)
        herder = Herder()
        versions = herder.get_versions()
        for v in versions[:-1]:
            herder.apply_migration(v)
        initial_state = self._get_comparables(mig_db)
        herder.apply_migration(versions[-1])
        herder.revert_migration(versions[-1])
        final_state = self._get_comparables(mig_db)
        assert initial_state == final_state

    def test_all_down_migrations(self, mig_db: orm.Database):
        """test running all down migrations

        this is largely to keep the coverage pct high, but also to make sure
        that we *can* run all down migrations (i.e. nothing got messed up by
        later migrations)
        """
        mig_db.drop_all_tables(with_all_data=True)
        herder = Herder()
        herder.execute()
        herder.execute("down")
        mig_schema = self._get_comparables(mig_db)
        assert mig_schema == {"table": {}, "index": {}, "view": {}, "trigger": {}}
