import random
import uuid
from datetime import datetime, timezone
from typing import Any, Optional
from unittest.mock import Mock

from pony.orm import MultipleObjectsFoundError

from between_words.db_models import User, db

ALPHA_NUM = "abcdefghijklmnopqrstuvwxyz" + "ABCDEFGHIJKLMNOPQRSTUVWXYZ" + "0123456789"


def create_mock_entity_cls(
    entity_cls: db.Entity,  # type: ignore
    default_values: dict[str, Any],
):
    all_mock_entities: list[Mock] = []

    def _create_mock_entity(**attr_values) -> Mock:
        nonlocal all_mock_entities
        now = datetime.now(timezone.utc)
        mock_entity = Mock(entity_cls)
        for attr in entity_cls._attrs_:
            value = default_values.get(attr.name, None)
            if attr.name in attr_values:
                value = attr_values[attr.name]
            elif attr.name == "uuid":
                value = uuid.uuid4
            elif attr.name in ("created_at", "updated_at"):
                value = now
            if callable(value):
                value = value()
            setattr(mock_entity, attr.name, value)
        all_mock_entities.append(mock_entity)
        return mock_entity

    def _get_mock_entity(**attr_values) -> Optional[Mock]:
        nonlocal all_mock_entities
        matched_mocks: list[Mock] = []
        for e in all_mock_entities:
            for attr, value in attr_values.items():
                if getattr(e, attr) != value:
                    break
            else:
                matched_mocks.append(e)
        if len(matched_mocks) > 1:
            raise MultipleObjectsFoundError
        if len(matched_mocks) == 0:
            return None
        return matched_mocks[0]

    def _get_all_entities() -> list[Mock]:
        nonlocal all_mock_entities
        return all_mock_entities

    def _reset_mock():
        nonlocal all_mock_entities
        all_mock_entities = []

    mock_cls = Mock(
        entity_cls,
        side_effect=_create_mock_entity,
        select=Mock(side_effect=_get_all_entities),
        get=Mock(side_effect=_get_mock_entity),
        reset_mock_entities=Mock(side_effect=_reset_mock),
    )

    return mock_cls


def rand_alnum():
    return "".join(random.choices(ALPHA_NUM, k=random.randint(5, 20)))


MockUser = create_mock_entity_cls(
    User,
    {
        "username": rand_alnum,
        "password_hash": rand_alnum,
        "favorite_fruit": "lemon",
    },
)
