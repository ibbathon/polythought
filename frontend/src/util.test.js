import { buildApiRequest } from "@root/util";

describe("buildApiRequest", () => {
  beforeEach(() => {
    fetch.resetMocks();
  });

  it("sends JSON data in JSON request", () => {
    fetch.mockResponse(JSON.stringify({}));

    const apiRequest = buildApiRequest({});
    apiRequest("/testurl", "POST", { test: "data" });

    expect(fetch).toHaveBeenCalledWith("/testapi/testurl", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: '{"test":"data"}',
    });
  });

  it("sends form data in form request", () => {
    fetch.mockResponse(JSON.stringify({}));

    const apiRequest = buildApiRequest({});
    apiRequest("/testurl", "POST", { test: "data" }, "form");

    const expectedFormData = new FormData();
    expectedFormData.append("test", "data");
    expect(fetch).toHaveBeenCalledWith("/testapi/testurl", {
      method: "POST",
      headers: {},
      body: expectedFormData,
    });
  });

  it("uses authtoken if present", () => {
    fetch.mockResponse(JSON.stringify({}));

    const apiRequest = buildApiRequest({ authtoken: "testtoken" });
    apiRequest("/testurl", "GET");

    expect(fetch).toHaveBeenCalledWith("/testapi/testurl", {
      method: "GET",
      headers: { Authorization: "Bearer testtoken" },
    });
  });

  it("returns JSON response data on successful fetch", async () => {
    fetch.mockResponse(JSON.stringify({ response: "data" }));
    const apiRequest = buildApiRequest({});

    const response = await apiRequest("/testurl", "GET");

    expect(response).toEqual({ response: "data" });
  });

  it("logs out and navigates to login if fetch 401'd and methods provided", async () => {
    const mockLogout = jest.fn();
    const mockNavigate = jest.fn();
    fetch.mockResponse(JSON.stringify({}), { status: 401 });

    const apiRequest = buildApiRequest({}, mockLogout, mockNavigate);
    await apiRequest("/testurl", "GET");

    expect(mockLogout).toHaveBeenCalledWith();
    expect(mockNavigate).toHaveBeenCalledWith("/login");
  });

  it("raises an error if 401'd and methods not provided", async () => {
    fetch.mockResponse(JSON.stringify({ detail: "testerror" }), {
      status: 401,
    });

    const apiRequest = buildApiRequest({});
    expect.assertions(1);
    try {
      await apiRequest("/testurl", "GET");
    } catch (err) {
      expect(err).toEqual("testerror");
    }
  });

  it("raises an error if any other error encountered", async () => {
    const mockLogout = jest.fn();
    const mockNavigate = jest.fn();
    fetch.mockResponse(JSON.stringify({ detail: "testnotfound" }), {
      status: 404,
    });

    const apiRequest = buildApiRequest({}, mockLogout, mockNavigate);
    expect.assertions(3);
    try {
      await apiRequest("/testurl", "GET");
    } catch (err) {
      expect(err).toEqual("testnotfound");
    }
    expect(mockLogout).not.toHaveBeenCalled();
    expect(mockNavigate).not.toHaveBeenCalled();
  });
});
