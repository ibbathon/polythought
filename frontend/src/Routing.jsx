import { Route, Router } from "@solidjs/router";
import { lazy } from "solid-js";

import { StoreProvider } from "@root/store";

import App from "@components/App";

const LoginPage = lazy(() => import("@pages/Login"));
const LogoutPage = lazy(() => import("@pages/Logout"));
const ProfilePage = lazy(() => import("@pages/Profile"));
const UserPage = lazy(() => import("@pages/User"));
const RandomWordPage = lazy(() => import("@pages/Word"));

function StoreWrappedApp(props) {
  return (
    <StoreProvider>
      <App>{props.children}</App>
    </StoreProvider>
  );
}

export default function Routing(_props) {
  return (
    <Router root={StoreWrappedApp}>
      <Route path="/login" component={LoginPage} />
      <Route path="/logout" component={LogoutPage} />
      <Route path="/profile" component={ProfilePage} />
      <Route path="/users/:username?" component={UserPage} />
      <Route path="/words" component={RandomWordPage} />
      <Route path="/" component={App} />
    </Router>
  );
}
