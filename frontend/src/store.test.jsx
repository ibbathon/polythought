import { useNavigate } from "@solidjs/router";
import { useContext } from "solid-js";
import { render } from "solid-testing-library";

import { StoreContext, StoreProvider } from "@root/store";
import { buildApiRequest } from "@root/util";

jest.mock("@solidjs/router");
jest.mock("@root/util");
const mockUseNavigate = useNavigate;
const mockNavigate = jest.fn();
mockUseNavigate.mockReturnValue(mockNavigate);
const mockBuildApiRequest = buildApiRequest;

describe("StoreProvider", () => {
  function getStore() {
    let store, actions;
    function MockStoreUser() {
      [store, actions] = useContext(StoreContext);
      return <></>;
    }
    render(
      <StoreProvider>
        <MockStoreUser />
      </StoreProvider>,
    );
    return [store, actions];
  }

  beforeEach(() => {
    jest.clearAllMocks();
    localStorage.clear();
  });

  it("reads initial auth info from localStorage", () => {
    localStorage.setItem("authtoken", "teststarttoken");
    localStorage.setItem("authuser", "teststartuser");
    const [store] = getStore();

    expect(store.authtoken).toEqual("teststarttoken");
    expect(store.authuser).toEqual("teststartuser");
  });

  describe("login function", () => {
    it("sets localStorage and store on successful call", async () => {
      const mockAuthApiRequest = jest.fn();
      mockAuthApiRequest.mockResolvedValue({ access_token: "testtoken" });
      mockBuildApiRequest.mockReturnValue(mockAuthApiRequest);
      const [store, { login }] = getStore();

      login("testuser", "testpassword");

      expect(mockAuthApiRequest).toHaveBeenCalledWith(
        "/auth/login",
        "POST",
        {
          grant_type: "password",
          username: "testuser",
          password: "testpassword",
        },
        "form",
      );
      await new Promise(process.nextTick);
      expect(store.authuser).toEqual("testuser");
      expect(store.authtoken).toEqual("testtoken");
      expect(localStorage.getItem("authuser")).toEqual("testuser");
      expect(localStorage.getItem("authtoken")).toEqual("testtoken");
    });

    it("does not set localStorage and store on failed call", async () => {
      const mockAuthApiRequest = jest.fn();
      mockAuthApiRequest.mockRejectedValue("failed");
      mockBuildApiRequest.mockReturnValue(mockAuthApiRequest);
      const [store, { login }] = getStore();

      expect(login("testuser", "testpassword")).rejects.toEqual("failed");

      await new Promise(process.nextTick);
      expect(store.authuser).toEqual(null);
      expect(store.authtoken).toEqual(null);
      expect(localStorage.getItem("authuser")).toEqual(null);
      expect(localStorage.getItem("authtoken")).toEqual(null);
    });
  });

  describe("logout function", () => {
    it("resets localStorage and store on successful call", async () => {
      localStorage.setItem("authtoken", "teststarttoken");
      localStorage.setItem("authuser", "teststartuser");
      const mockAuthApiRequest = jest.fn();
      mockAuthApiRequest.mockResolvedValue({});
      mockBuildApiRequest.mockReturnValue(mockAuthApiRequest);
      const [store, { logout }] = getStore();

      logout();

      expect(mockAuthApiRequest).toHaveBeenCalledWith("/auth/logout", "POST");
      await new Promise(process.nextTick);
      expect(store.authuser).toEqual(null);
      expect(store.authtoken).toEqual(null);
      expect(localStorage.getItem("authuser")).toEqual(null);
      expect(localStorage.getItem("authtoken")).toEqual(null);
    });

    it("resets localStorage and store on failed call", async () => {
      localStorage.setItem("authtoken", "teststarttoken");
      localStorage.setItem("authuser", "teststartuser");
      const mockAuthApiRequest = jest.fn();
      mockAuthApiRequest.mockRejectedValue("failed");
      mockBuildApiRequest.mockReturnValue(mockAuthApiRequest);
      const [store, { logout }] = getStore();

      expect(logout()).rejects.toEqual("failed");

      await new Promise(process.nextTick);
      expect(store.authuser).toEqual(null);
      expect(store.authtoken).toEqual(null);
      expect(localStorage.getItem("authuser")).toEqual(null);
      expect(localStorage.getItem("authtoken")).toEqual(null);
    });
  });

  it("provides safe apiRequest method", () => {});
});
