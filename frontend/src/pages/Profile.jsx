import { createResource } from "solid-js";
import { useContext } from "solid-js";

import { StoreContext } from "@root/store";

import UserComponent from "@components/UserComponent";

export default function ProfilePage() {
  const [store, { apiRequest }] = useContext(StoreContext);
  const getSelf = async () => apiRequest(`/users/${store.authuser}`, "GET");
  const [selfData] = createResource(() => store.authuser, getSelf);
  return (
    <>
      <Show when={selfData()}>
        <UserComponent user={selfData()} />
      </Show>
    </>
  );
}
