import { useContext } from "solid-js";
import { render } from "solid-testing-library";

import UserComponent from "@components/UserComponent";

import ProfilePage from "@pages/Profile";

jest.mock("solid-js", () => ({
  ...jest.requireActual("solid-js"),
  useContext: jest.fn(),
}));
jest.mock("@components/UserComponent");
const mockUseContext = useContext;
const mockApiRequest = jest.fn(() => "testresponse");
mockUseContext.mockReturnValue([
  { authuser: "testuser" },
  { apiRequest: mockApiRequest },
]);
const mockUserComponent = UserComponent;

describe("ProfilePage", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("renders UserComponent for provided data", async () => {
    render(() => <ProfilePage />);
    expect(mockApiRequest).toHaveBeenCalledWith("/users/testuser", "GET");
    await new Promise(process.nextTick);
    expect(mockUserComponent).toHaveBeenCalledWith({ user: "testresponse" });
  });
});
