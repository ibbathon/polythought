import { useNavigate } from "@solidjs/router";
import { useContext } from "solid-js";
import { render } from "solid-testing-library";

import LogoutPage from "@pages/Logout";

jest.mock("@solidjs/router");
jest.mock("solid-js", () => ({
  ...jest.requireActual("solid-js"),
  useContext: jest.fn(),
}));
const mockUseNavigate = useNavigate;
const mockNavigate = jest.fn();
mockUseNavigate.mockReturnValue(mockNavigate);
const mockUseContext = useContext;
const mockLogout = jest.fn();
mockLogout.mockResolvedValue({});
mockUseContext.mockReturnValue([, { logout: mockLogout }]);

describe("LogoutPage", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("calls logout and navigates home on render", async () => {
    render(() => <LogoutPage />);
    expect(mockLogout).toHaveBeenCalled();
    // this await gives the logout enough time to fully resolve
    await new Promise(process.nextTick);
    expect(mockNavigate).toHaveBeenCalledWith("/");
  });
});
