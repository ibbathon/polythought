import { useNavigate } from "@solidjs/router";
import userEvent from "@testing-library/user-event";
import { useContext } from "solid-js";
import { fireEvent, render } from "solid-testing-library";

import LoginPage from "@pages/Login";

jest.mock("@solidjs/router");
jest.mock("solid-js", () => ({
  ...jest.requireActual("solid-js"),
  useContext: jest.fn(),
}));
const mockUseContext = useContext;
const mockLogin = jest.fn();
mockUseContext.mockReturnValue([, { login: mockLogin }]);
const mockUseNavigate = useNavigate;
const mockNavigate = jest.fn();
mockUseNavigate.mockReturnValue(mockNavigate);
const user = userEvent.setup();

describe("LoginPage", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("sends entered text and navigates on success", async () => {
    mockLogin.mockResolvedValue({});
    const { getByTestId } = render(() => <LoginPage />);

    const usernameEntry = getByTestId("username");
    const passwordEntry = getByTestId("password");
    const loginButton = getByTestId("login");
    fireEvent.change(usernameEntry, { target: { value: "testusername" } });
    fireEvent.change(passwordEntry, { target: { value: "testpassword" } });
    await user.click(loginButton);

    expect(mockLogin).toHaveBeenCalledWith("testusername", "testpassword");
    await new Promise(process.nextTick);
    expect(mockNavigate).toHaveBeenCalledWith("/profile");
  });

  it("displays raw-text errors", async () => {
    mockLogin.mockRejectedValue("testerror");
    const { getByTestId, findByText } = render(() => <LoginPage />);

    const loginButton = getByTestId("login");
    await user.click(loginButton);
    await new Promise(process.nextTick);

    expect(await findByText("testerror")).toBeInTheDocument();
    expect(mockNavigate).not.toHaveBeenCalled();
  });

  it("displays JSON errors", async () => {
    mockLogin.mockRejectedValue([
      { msg: "err1", loc: ["some", "where"] },
      { msg: "err2", loc: ["else"] },
    ]);
    const { getByTestId, findByText } = render(() => <LoginPage />);

    const loginButton = getByTestId("login");
    await user.click(loginButton);
    await new Promise(process.nextTick);

    expect(await findByText("err1: some.where")).toBeInTheDocument();
    expect(await findByText("err2: else")).toBeInTheDocument();
    expect(mockNavigate).not.toHaveBeenCalled();
  });

  it("does not respond to second click while processing", async () => {
    mockLogin.mockReturnValue(new Promise((r) => setTimeout(r, 500)));
    const { getByTestId } = render(() => <LoginPage />);

    const loginButton = getByTestId("login");
    await user.click(loginButton);
    await user.click(loginButton);
    await new Promise(process.nextTick);

    expect(mockLogin).toHaveBeenCalledTimes(1);
  });
});
