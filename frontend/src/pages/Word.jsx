import { createResource, useContext } from "solid-js";
import { createStore } from "solid-js/store";

import { StoreContext } from "@root/store";

export default function RandomWordPage() {
  const [formData, setFormData] = createStore({
    count: 1,
    dictionary: "default",
  });
  const [, { apiRequest }] = useContext(StoreContext);
  const getWords = async () => {
    setFormData({ errors: null });
    return apiRequest(
      `/words/random?count=${formData.count}&dictionary=${formData.dictionary}`,
      "GET",
    ).catch((errors) => setFormData({ errors }));
  };
  const [randomWords, { refetch }] = createResource(getWords);
  const refetchPreventDefault = (e) => {
    e.preventDefault();
    refetch();
  };

  return (
    <>
      <form onSubmit={refetchPreventDefault}>
        <input
          type="text"
          placeholder="count"
          title="count"
          data-testid="count"
          onChange={(e) => setFormData({ count: e.target.value })}
          value={formData.count}
        />
        <input
          type="text"
          placeholder="dictionary"
          title="dictionary"
          data-testid="dictionary"
          onChange={(e) => setFormData({ dictionary: e.target.value })}
          value={formData.dictionary}
        />
        <button type="submit" data-testid="getwords" title="get words">
          get words
        </button>
      </form>
      <Show when={randomWords()}>
        <For each={randomWords()}>{(word) => <div>{word}</div>}</For>
      </Show>
      <Show when={formData.errors}>Errors: {formData.errors}</Show>
    </>
  );
}
