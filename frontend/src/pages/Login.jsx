import { useNavigate } from "@solidjs/router";
import { useContext } from "solid-js";
import { createStore } from "solid-js/store";

import { StoreContext } from "@root/store";

export default function LoginPage() {
  const [, { login }] = useContext(StoreContext);
  const [formData, setFormData] = createStore({
    username: "",
    password: "",
    processing: false,
  });
  const navigate = useNavigate();

  const handleLogin = (e) => {
    e.preventDefault();
    setFormData({ processing: true });
    login(formData.username, formData.password)
      .then(() => navigate("/profile"))
      .catch((errors) => setFormData({ errors }))
      .finally(() => setFormData({ processing: false }));
  };

  return (
    <>
      {/* raw-text errors (e.g. from OAuth) */}
      <Show when={formData.errors && typeof formData.errors === "string"}>
        <div>{formData.errors}</div>
      </Show>
      {/* JSON errors from FastAPI */}
      <Show when={formData.errors && typeof formData.errors !== "string"}>
        <For each={Object.keys(formData.errors)}>
          {(key) => {
            const error = formData.errors[key];
            return (
              <div>
                {error.msg}: {error.loc.join(".")}
              </div>
            );
          }}
        </For>
      </Show>

      <form onSubmit={handleLogin}>
        <input
          type="text"
          data-testid="username"
          placeholder="username"
          value={formData.username}
          onChange={(e) => setFormData({ username: e.target.value })}
        />
        <input
          type="password"
          data-testid="password"
          placeholder="password"
          value={formData.password}
          onChange={(e) => setFormData({ password: e.target.value })}
        />
        <button
          type="submit"
          data-testid="login"
          disabled={formData.processing}
          textContent="Login"
        />
      </form>
    </>
  );
}
