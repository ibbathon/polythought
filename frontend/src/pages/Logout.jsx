import { useNavigate } from "@solidjs/router";
import { useContext } from "solid-js";

import { StoreContext } from "@root/store";

export default function LogoutPage() {
  const [, { logout }] = useContext(StoreContext);
  const navigate = useNavigate();
  logout().finally(() => navigate("/"));
  return <></>;
}
