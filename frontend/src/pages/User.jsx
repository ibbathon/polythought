import { useParams } from "@solidjs/router";
import { createResource } from "solid-js";
import { useContext } from "solid-js";

import { StoreContext } from "@root/store";

import UserComponent from "@components/UserComponent";

export default function UserPage() {
  const [, { apiRequest }] = useContext(StoreContext);
  const params = useParams();
  if (params.username) {
    const getUser = async () => apiRequest(`/users/${params.username}`, "GET");
    const [userData] = createResource(() => params.username, getUser);
    return (
      <>
        <Show when={userData()}>
          <UserComponent user={userData()} />
        </Show>
      </>
    );
  } else {
    const getUsers = async () => apiRequest("/users", "GET");
    const [userData] = createResource(getUsers);
    return (
      <>
        <Show when={userData()}>
          <For each={userData()}>{(user) => <UserComponent user={user} />}</For>
        </Show>
      </>
    );
  }
}
