import userEvent from "@testing-library/user-event";
import { useContext } from "solid-js";
import { fireEvent, render } from "solid-testing-library";

import RandomWordPage from "@pages/Word";

jest.mock("@solidjs/router");
jest.mock("solid-js", () => ({
  ...jest.requireActual("solid-js"),
  useContext: jest.fn(),
}));
const mockUseContext = useContext;
const mockApiRequest = jest.fn();
mockUseContext.mockReturnValue([, { apiRequest: mockApiRequest }]);

const user = userEvent.setup();

describe("RandomWordPage", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("defaults count and dictionary", async () => {
    mockApiRequest.mockResolvedValue(["blah"]);

    const { getByTestId } = render(() => <RandomWordPage />);

    const getWordsButton = getByTestId("getwords");
    await user.click(getWordsButton);

    expect(mockApiRequest).toHaveBeenLastCalledWith(
      "/words/random?count=1&dictionary=default",
      "GET",
    );
  });

  it("sends entered text and renders words on success", async () => {
    mockApiRequest.mockResolvedValue(["blah", "other", "thing"]);

    const { getByTestId, findByText } = render(() => <RandomWordPage />);

    const countEntry = getByTestId("count");
    const dictEntry = getByTestId("dictionary");
    const getWordsButton = getByTestId("getwords");
    fireEvent.change(countEntry, { target: { value: "5" } });
    fireEvent.change(dictEntry, { target: { value: "another" } });
    await user.click(getWordsButton);

    expect(mockApiRequest).toHaveBeenLastCalledWith(
      "/words/random?count=5&dictionary=another",
      "GET",
    );

    expect(await findByText("blah")).toBeInTheDocument();
    expect(await findByText("other")).toBeInTheDocument();
    expect(await findByText("thing")).toBeInTheDocument();
  });

  it("displays errors", async () => {
    mockApiRequest.mockRejectedValue("somesuch");

    const { getByTestId, findByText } = render(() => <RandomWordPage />);

    const getWordsButton = getByTestId("getwords");
    await user.click(getWordsButton);

    expect(await findByText("Errors: somesuch")).toBeInTheDocument();
  });
});
