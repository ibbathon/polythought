import { useParams } from "@solidjs/router";
import { useContext } from "solid-js";
import { render } from "solid-testing-library";

import UserComponent from "@components/UserComponent";

import UserPage from "@pages/User";

jest.mock("@solidjs/router");
jest.mock("solid-js", () => ({
  ...jest.requireActual("solid-js"),
  useContext: jest.fn(),
}));
jest.mock("@components/UserComponent");
const mockUseParams = useParams;
const mockUseContext = useContext;
const mockApiRequest = jest.fn();
mockUseContext.mockReturnValue([, { apiRequest: mockApiRequest }]);
const mockUserComponent = UserComponent;

describe("UserPage", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("renders requested user when requested", async () => {
    mockUseParams.mockReturnValue({ username: "testuser" });
    mockApiRequest.mockReturnValue({ test: "user" });
    render(() => <UserPage />);
    expect(mockApiRequest).toHaveBeenCalledWith("/users/testuser", "GET");
    await new Promise(process.nextTick);
    expect(mockUserComponent).toHaveBeenCalledWith({ user: { test: "user" } });
  });

  it("renders list of users when no user requested", async () => {
    mockUseParams.mockReturnValue({});
    mockApiRequest.mockReturnValue([{ test: "user1" }, { test: "user2" }]);
    render(() => <UserPage />);
    expect(mockApiRequest).toHaveBeenCalledWith("/users", "GET");
    await new Promise(process.nextTick);
    expect(mockUserComponent).toHaveBeenCalledWith({ user: { test: "user1" } });
    expect(mockUserComponent).toHaveBeenCalledWith({ user: { test: "user2" } });
  });
});
