import { urlBase } from "@root/constants";

export function buildApiRequest(store, logout, navigate) {
  return async function apiRequest(url, method, data, dataFormat) {
    const requestData = { method, headers: {} };

    if (data) {
      if (dataFormat === "form") {
        const formdata = new FormData();
        for (let k in data) {
          formdata.append(k, data[k]);
        }
        requestData.body = formdata;
      } else {
        requestData.headers["Content-Type"] = "application/json";
        requestData.body = JSON.stringify(data);
      }
    }
    if (store.authtoken) {
      requestData.headers["Authorization"] = `Bearer ${store.authtoken}`;
    }

    const response = await fetch(urlBase + url, requestData);
    const responseData = await response.json();
    if (response.status < 300) {
      return responseData;
    } else if (response.status === 401 && logout && navigate) {
      logout();
      navigate("/login");
    } else {
      throw responseData.detail;
    }
  };
}
