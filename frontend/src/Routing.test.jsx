import { useNavigate } from "@solidjs/router";
import { render } from "solid-testing-library";

import Routing from "@root/Routing";
import { StoreProvider } from "@root/store";

import App from "@components/App";

import LoginPage from "@pages/Login";
import LogoutPage from "@pages/Logout";
import ProfilePage from "@pages/Profile";
import UserPage from "@pages/User";
import RandomWordPage from "@pages/Word";

// the router's `navigate` method calls window.scrollTo, but that's not defined
// for tests, so mock it out
window.scrollTo = () => {};

jest.mock("@root/store");
jest.mock("@pages/Login");
jest.mock("@pages/Logout");
jest.mock("@pages/Profile");
jest.mock("@pages/User");
jest.mock("@pages/Word");
jest.mock("@components/App");
const mockStoreProvider = StoreProvider;
const mockLoginPage = LoginPage;
const mockLogoutPage = LogoutPage;
const mockProfilePage = ProfilePage;
const mockUserPage = UserPage;
const mockRandomWordPage = RandomWordPage;
const mockApp = App;

let navigate;
mockStoreProvider.mockImplementation((props) => (
  <>
    <div>"teststore"</div>
    {props.children}
  </>
));
mockApp.mockImplementation((props) => {
  navigate = useNavigate();
  return (
    <>
      <div>"testapp"</div>
      {props.children}
    </>
  );
});

describe("Routing", () => {
  beforeEach(() => {
    if (navigate) {
      // solidjs/router has some sort of global context that preserves the
      // current location, and then re-renders the current page before navigating
      // elsewhere; so we need to force it to start from / every time
      navigate("/");
    }
    jest.clearAllMocks();
  });

  it("root navigation", async () => {
    render(() => <Routing />);
    expect(mockStoreProvider).toHaveBeenCalledTimes(1);
    expect(mockApp).toHaveBeenCalledTimes(2);
  });

  it("login navigation", async () => {
    render(() => <Routing />);
    navigate("/login");
    await new Promise(process.nextTick);
    expect(mockLoginPage).toHaveBeenCalledTimes(1);
  });

  it("logout navigation", async () => {
    render(() => <Routing />);
    navigate("/logout");
    await new Promise(process.nextTick);
    expect(mockLogoutPage).toHaveBeenCalledTimes(1);
  });

  it("profile navigation", async () => {
    render(() => <Routing />);
    navigate("/profile");
    await new Promise(process.nextTick);
    expect(mockProfilePage).toHaveBeenCalledTimes(1);
  });

  it("users navigation", async () => {
    render(() => <Routing />);
    navigate("/users");
    await new Promise(process.nextTick);
    expect(mockUserPage).toHaveBeenCalledTimes(1);
    expect(mockUserPage.mock.lastCall[0].params).toEqual({});
  });

  it("random words navigation", async () => {
    render(() => <Routing />);
    navigate("/words");
    await new Promise(process.nextTick);
    expect(mockRandomWordPage).toHaveBeenCalledTimes(1);
    expect(mockRandomWordPage.mock.lastCall[0].params).toEqual({});
  });

  it("specific user navigation", async () => {
    render(() => <Routing />);
    navigate("/users/test");
    await new Promise(process.nextTick);
    expect(mockUserPage).toHaveBeenCalledTimes(1);
    expect(mockUserPage.mock.lastCall[0].params).toEqual({ username: "test" });
  });

  it("only renders the requested page", async () => {
    // this test is to validate that the stupid workaround in the beforeEach
    // still functions; I don't think I'm going to be using SolidJS for any
    // future projects
    render(() => <Routing />);
    navigate("/login");
    await new Promise(process.nextTick);
    expect(mockApp).toHaveBeenCalledTimes(2);
    expect(mockStoreProvider).toHaveBeenCalledTimes(1);
    expect(mockLoginPage).toHaveBeenCalledTimes(1);
    expect(mockLogoutPage).not.toHaveBeenCalled();
    expect(mockProfilePage).not.toHaveBeenCalled();
    expect(mockUserPage).not.toHaveBeenCalled();
  });
});
