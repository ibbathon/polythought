import { useNavigate } from "@solidjs/router";
import { useContext } from "solid-js";

import { StoreContext } from "@root/store";

export default function LogoutButton() {
  const [, { logout }] = useContext(StoreContext);
  const navigate = useNavigate();
  const handleLogout = (e) => {
    e.preventDefault();
    logout().finally(() => navigate("/"));
  };

  return (
    <button type="submit" onClick={handleLogout}>
      Logout
    </button>
  );
}
