import { useNavigate } from "@solidjs/router";
import userEvent from "@testing-library/user-event";
import { useContext } from "solid-js";
import { render } from "solid-testing-library";

import LogoutButton from "@components/LogoutButton";

jest.mock("@solidjs/router");
jest.mock("solid-js", () => ({
  ...jest.requireActual("solid-js"),
  useContext: jest.fn(),
}));
const mockUseNavigate = useNavigate;
const mockNavigate = jest.fn();
mockUseNavigate.mockReturnValue(mockNavigate);
const mockUseContext = useContext;
const mockLogout = jest.fn();
mockLogout.mockResolvedValue({});
mockUseContext.mockReturnValue([, { logout: mockLogout }]);
const user = userEvent.setup();

describe("LogoutButton", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("calls logout and navigates home on click", async () => {
    const { getByRole } = render(() => <LogoutButton />);
    const button = getByRole("button");
    await user.click(button);
    expect(mockLogout).toHaveBeenCalled();
    expect(mockNavigate).toHaveBeenCalledWith("/");
  });
});
