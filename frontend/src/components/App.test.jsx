import * as solidRouter from "@solidjs/router";
import { useContext } from "solid-js";
import { render } from "solid-testing-library";

import App from "@components/App";
import LogoutButton from "@components/LogoutButton";

jest.mock("@solidjs/router");
jest.mock("@components/LogoutButton");
jest.mock("solid-js", () => ({
  ...jest.requireActual("solid-js"),
  useContext: jest.fn(),
}));
const mockLogoutButton = LogoutButton;
const mockUseContext = useContext;
const mockUseLocation = solidRouter.useLocation;
const mockUseNavigate = solidRouter.useNavigate;

describe("App", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("navigates to /login if unauthenticated and not on /login", async () => {
    const mockNavigate = jest.fn();
    mockUseLocation.mockReturnValue({ pathname: "/other" });
    mockUseNavigate.mockReturnValue(mockNavigate);
    mockUseContext.mockReturnValue([{ authtoken: undefined }]);

    render(() => <App />);

    expect(mockNavigate).toHaveBeenCalledWith("/login");
  });

  it("does not render logout button inside unauthenticated App", () => {
    const mockNavigate = jest.fn();
    mockUseLocation.mockReturnValue({ pathname: "/login" });
    mockUseNavigate.mockReturnValue(mockNavigate);
    mockUseContext.mockReturnValue([{ authtoken: undefined }]);

    render(() => <App />);

    expect(mockLogoutButton).not.toHaveBeenCalled();
    expect(mockNavigate).not.toHaveBeenCalled();
  });

  it("renders logout button inside authenticated App", () => {
    mockUseContext.mockReturnValue([{ authtoken: true }]);
    render(() => <App />);
    expect(mockLogoutButton.mock.calls).toEqual([[{}]]);
  });
});
