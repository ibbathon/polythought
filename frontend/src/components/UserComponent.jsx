export default function UserComponent(props) {
  return (
    <>
      <div>Username: {props.user.username}</div>
      <div>Created: {props.user.created_at}</div>
      <div>Last Updated: {props.user.updated_at}</div>
      {props.user.favorite_fruit && (
        <div>Favorite Fruit: {props.user.favorite_fruit}</div>
      )}
    </>
  );
}
