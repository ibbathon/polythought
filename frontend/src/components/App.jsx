import { useLocation, useNavigate } from "@solidjs/router";
import { useContext } from "solid-js";

import { StoreContext } from "@root/store";

import LogoutButton from "@components/LogoutButton";

export default function App(props) {
  const [store] = useContext(StoreContext);
  const navigate = useNavigate();
  const location = useLocation();
  if (!store.authtoken && !/\/login/.test(location.pathname)) {
    navigate("/login");
  }

  return (
    <div>
      <h1>Welcome</h1>
      {store.authtoken && (
        <>
          <LogoutButton />
        </>
      )}
      {props.children}
    </div>
  );
}
