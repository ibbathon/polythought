import { render } from "solid-testing-library";

import UserComponent from "@components/UserComponent";

describe("UserComponent", () => {
  it("displays all details when provided", async () => {
    const user = {
      username: "testname",
      created_at: "testcreated",
      updated_at: "testupdated",
      favorite_fruit: "testfruit",
    };
    const { findByText } = render(() => <UserComponent user={user} />);
    expect(await findByText("Username: testname")).toBeInTheDocument();
    expect(await findByText("Created: testcreated")).toBeInTheDocument();
    expect(await findByText("Last Updated: testupdated")).toBeInTheDocument();
    expect(await findByText("Favorite Fruit: testfruit")).toBeInTheDocument();
  });

  it("does not display fruit when not provided", async () => {
    const user = {
      username: "testname",
      created_at: "testcreated",
      updated_at: "testupdated",
    };
    const { findByText, queryByText } = render(() => (
      <UserComponent user={user} />
    ));
    expect(await findByText("Username: testname")).toBeInTheDocument();
    expect(await findByText("Created: testcreated")).toBeInTheDocument();
    expect(await findByText("Last Updated: testupdated")).toBeInTheDocument();
    expect(
      await queryByText("Favorite Fruit: testfruit"),
    ).not.toBeInTheDocument();
  });
});
