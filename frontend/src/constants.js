export const urlBase = import.meta.env.VITE_LOCAL_API_URL
  ? import.meta.env.VITE_LOCAL_API_URL
  : "/api";
