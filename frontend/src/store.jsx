import { useNavigate } from "@solidjs/router";
import { createContext } from "solid-js";
import { createStore } from "solid-js/store";

import { buildApiRequest } from "@root/util";

export const StoreContext = createContext();
export function StoreProvider(props) {
  const [store, setStore] = createStore({
    authuser: localStorage.getItem("authuser"),
    authtoken: localStorage.getItem("authtoken"),
  });
  const storeActions = {};
  const authApiRequest = buildApiRequest(store);

  storeActions.login = (username, password) => {
    const result = authApiRequest(
      "/auth/login",
      "POST",
      { grant_type: "password", username, password },
      "form",
    );
    return result.then((data) => {
      localStorage.setItem("authtoken", data.access_token);
      localStorage.setItem("authuser", username);
      setStore({ authuser: username, authtoken: data.access_token });
    });
  };
  storeActions.logout = () => {
    const result = authApiRequest("/auth/logout", "POST");
    return result.finally(() => {
      localStorage.removeItem("authtoken");
      localStorage.removeItem("authuser");
      setStore({ authuser: null, authtoken: null });
    });
  };

  const navigate = useNavigate();
  storeActions.apiRequest = buildApiRequest(
    store,
    storeActions.logout,
    navigate,
  );

  return (
    <StoreContext.Provider value={[store, storeActions]}>
      {props.children}
    </StoreContext.Provider>
  );
}
